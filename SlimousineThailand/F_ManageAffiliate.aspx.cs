﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SlimousineThailand
{
    public partial class F_ManageAffiliate : System.Web.UI.Page
    {
        static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
        private DataSet ds;
        SqlCommand sqlcomm = new SqlCommand();
        static bool modeUpdate = false;
        static string AffiliateIdEdit = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Clear();
                Set_Grdaff();
                //Set_grdmemall(txt_seach_mem.Text);
                if (modeUpdate == true)
                {
                    Editaff("");
                }
            }
        }

        public void Set_Grdaff()
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        // This will be your input parameter and its value
                        //sqlcomm.Parameters.AddWithValue("@paramValue", paramValue);

                        sqlcomm.CommandText = "spr_SetGrd_Affiliate";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            grd_affdetial.DataSource = ds.Tables[0];
                            grd_affdetial.DataBind();
                        }
                        else
                        {
                            grd_affdetial.DataSource = null;
                            grd_affdetial.DataBind();

                        }

                    }


                }
                //catch (SQLException ex)
                //{
                //    Console.WriteLine("SQL Error: " + ex.Message);
                //}
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
            }
        }
        private void Insertaff()
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        // This will be your input parameter and its value
                        sqlcomm.Parameters.AddWithValue("@Name", txt_name_aff.Text);
                        sqlcomm.Parameters.AddWithValue("@Detail", txt_description_aff.Text);
                        sqlcomm.Parameters.AddWithValue("@Website", txt_web_aff.Text);
                        sqlcomm.Parameters.AddWithValue("@Adddress", txt_address_aff.Text);
                        sqlcomm.Parameters.AddWithValue("@Phone1", txt_tell1_aff.Text);
                        sqlcomm.Parameters.AddWithValue("@Phone2", txt_tell2_aff.Text);
                        sqlcomm.Parameters.AddWithValue("@Fax", txt_fax_aff.Text);
                        sqlcomm.Parameters.AddWithValue("@Email", txt_email_aff.Text);


                        sqlcomm.CommandText = "spr_Insert_Affiliate";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);

                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Record Inserted Successfully')", true);


                    }


                }
                //catch (SQLException ex)
                //{
                //    Console.WriteLine("SQL Error: " + ex.Message);
                //}
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
                Clear();
                Set_Grdaff();
            }
        }
        public void Editaff(string AffiliateIdEdit)
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        // This will be your input parameter and its value
                        sqlcomm.Parameters.AddWithValue("@IDAffiliate", AffiliateIdEdit);

                        sqlcomm.CommandText = "spr_Edit_Affiliate";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            modeUpdate = true;

                            txt_name_aff.Text = ds.Tables[0].Rows[0]["Name"].ToString();
                            txt_description_aff.Text = ds.Tables[0].Rows[0]["Detail"].ToString();
                            txt_web_aff.Text = ds.Tables[0].Rows[0]["Website"].ToString();
                            txt_email_aff.Text = ds.Tables[0].Rows[0]["Email"].ToString();
                            txt_tell1_aff.Text = ds.Tables[0].Rows[0]["Phone1"].ToString();
                            txt_tell2_aff.Text = ds.Tables[0].Rows[0]["Phone2"].ToString();
                            txt_fax_aff.Text = ds.Tables[0].Rows[0]["Fax"].ToString();
                            txt_address_aff.Text = ds.Tables[0].Rows[0]["Adddress"].ToString();

                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "showModal();", true);
                        }
                        else
                        {
                            modeUpdate = false;
                            this.Clear();
                        }

                    }


                }
                //catch (SQLException ex)
                //{
                //    Console.WriteLine("SQL Error: " + ex.Message);
                //}
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
            }
        }
        private void Updateaff(string AffiliateIdEdit)
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        // This will be your input parameter and its value
                        sqlcomm.Parameters.AddWithValue("@AffiliateID", AffiliateIdEdit);
                        sqlcomm.Parameters.AddWithValue("@Name", txt_name_aff.Text);
                        sqlcomm.Parameters.AddWithValue("@Detail", txt_description_aff.Text);
                        sqlcomm.Parameters.AddWithValue("@Website", txt_web_aff.Text);
                        sqlcomm.Parameters.AddWithValue("@Email", txt_email_aff.Text);
                        sqlcomm.Parameters.AddWithValue("@Phone1", txt_tell1_aff.Text);
                        sqlcomm.Parameters.AddWithValue("@Phone2", txt_tell2_aff.Text);
                        sqlcomm.Parameters.AddWithValue("@Fax", txt_fax_aff.Text);
                        sqlcomm.Parameters.AddWithValue("@Adddress", txt_address_aff.Text);


                        sqlcomm.CommandText = "spr_Update_Affiliate";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);

                        Set_Grdaff();
                        modeUpdate = false;

                    }


                }
                //catch (SQLException ex)
                //{
                //    Console.WriteLine("SQL Error: " + ex.Message);
                //}
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
            }
        }
        private void Clear()
        {
            txt_address_aff.Text = string.Empty;
            txt_description_aff.Text = string.Empty;
            txt_email_aff.Text = string.Empty;
            txt_fax_aff.Text = string.Empty;
            txt_tell1_aff.Text = string.Empty;
            txt_tell2_aff.Text = string.Empty;
            txt_web_aff.Text = string.Empty;
            txt_name_aff.Text = string.Empty;
        }

        protected void btn_add_aff_Click(object sender, EventArgs e)
        {
           ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "showModal();", true);
        }

        protected void btn_edit_aff_Click(object sender, EventArgs e)
        {
            AffiliateIdEdit = ((HiddenField)grd_affdetial.Rows[((GridViewRow)((LinkButton)sender).NamingContainer).RowIndex].FindControl("HaffID")).Value;
            Editaff(AffiliateIdEdit);
        }

        protected void btn_delete_aff_Click(object sender, EventArgs e)
        {
            string id = ((HiddenField)grd_affdetial.Rows[((GridViewRow)((LinkButton)sender).NamingContainer).RowIndex].FindControl("HaffID")).Value;

            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        // This will be your input parameter and its value
                        sqlcomm.Parameters.AddWithValue("@IDAffiliate", id);
                        sqlcomm.CommandText = "spr_Delete_Affiliate";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        Set_Grdaff();


                    }


                }

                catch (Exception ex)
                {
                    Console.WriteLine("Error: " + ex.Message);
                }
            }
        }

        protected void btn_Clear_aff_Click(object sender, EventArgs e)
        {
            Clear();
        }

        protected void btn_Save_aff_Click(object sender, EventArgs e)
        {
            int amount = 0;
            if (txt_name_aff.Text == "" && txt_description_aff.Text == "" && txt_address_aff.Text == "" && txt_web_aff.Text == "" && txt_email_aff.Text == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('กรุณากรอกข้อมูล')", true);
                //ShowMessage("Record submitted successfully", MessageType.Warning);
                return;
            }
            else
            {
                

            }
            if (modeUpdate == false)
            {
                Insertaff();
            }
            else
            {
                Updateaff(AffiliateIdEdit);
            }
        }
    }
}