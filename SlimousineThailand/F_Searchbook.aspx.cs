﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SlimousineThailand
{
    public partial class F_Searchbook : System.Web.UI.Page
    {
        static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
        private DataSet ds;
        SqlCommand sqlcomm = new SqlCommand();
        static string bookstatus = "";
        static string Datenow = "";
        static string ddl = "";
        static string IDbook = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Set_Grdseachall();
            }
        }
       
        
        public void Set_Grdseachdate()
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        // This will be your input parameter and its value
                        //sqlcomm.Parameters.AddWithValue("@paramValue", paramValue);
                        sqlcomm.Parameters.AddWithValue("@sdate", txt_sdate_SB.Text);
                        sqlcomm.Parameters.AddWithValue("@edate", txt_edate_SB.Text);

                        sqlcomm.CommandText = "spr_Seach_Seachdate";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            grd_searchbook.DataSource = ds.Tables[0];
                            grd_searchbook.DataBind();
                        }
                        else
                        {
                            grd_searchbook.DataSource = null;
                            grd_searchbook.DataBind();

                        }

                    }


                }
                //catch (SQLException ex)
                //{
                //    Console.WriteLine("SQL Error: " + ex.Message);
                //}
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
            }
        }
        public void Set_Grdseachall()
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        // This will be your input parameter and its value
                        //sqlcomm.Parameters.AddWithValue("@paramValue", paramValue);

                        sqlcomm.CommandText = "spr_SetGrid_Search";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            grd_searchbook.DataSource = ds.Tables[0];
                            grd_searchbook.DataBind();
                        }
                        else
                        {
                            grd_searchbook.DataSource = null;
                            grd_searchbook.DataBind();

                        }

                    }


                }
                //catch (SQLException ex)
                //{
                //    Console.WriteLine("SQL Error: " + ex.Message);
                //}
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
            }
        }

        protected void btn_search_SB_Click(object sender, EventArgs e)
        {
            
            if (txt_sdate_SB.Text != "" && txt_edate_SB.Text != "")
            {
                Set_Grdseachdate();
            }

        }

        protected void grd_searchbook_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                
                LinkButton statusa = (LinkButton)e.Row.FindControl("lbtn_a");
                LinkButton statusb = (LinkButton)e.Row.FindControl("lbtn_b");
                LinkButton statusc = (LinkButton)e.Row.FindControl("lbtn_c");
                LinkButton statusd = (LinkButton)e.Row.FindControl("lbtn_d");

                Label cach = (Label)e.Row.FindControl("lbl_cach2");
                Label cradit = (Label)e.Row.FindControl("lbl_cradit_card2");
                Label billing = (Label)e.Row.FindControl("lbl_biling2");
                Label mony = (Label)e.Row.FindControl("lbl_monytranfer2");

                HiddenField hid_status = (e.Row.FindControl("Hid_bkdsstatus") as HiddenField);
                HiddenField Hid_Peyments2 = (e.Row.FindControl("Hid_Peyments2") as HiddenField);

                if (hid_status.Value == "Pending")
                {

                    statusa.Visible = true;
                    statusb.Visible = false;
                    statusd.Visible = false;
                    statusc.Visible = false;
                }
                else if (hid_status.Value == "Comfirm")
                {
                    statusa.Visible = false;
                    statusb.Visible = true;
                    statusd.Visible = false;
                    statusc.Visible = false;
                }
                else if (hid_status.Value == "Complete")
                {
                    statusa.Visible = false;
                    statusb.Visible = false;
                    statusd.Visible = false;
                    statusc.Visible = true;
                }
                else
                {
                    statusa.Visible = false;
                    statusb.Visible = false;
                    statusd.Visible = true;
                    statusc.Visible = false;

                    
                }


                if (Hid_Peyments2.Value == "Billing")
                {

                    cach.Visible = false;
                    cradit.Visible = false;
                    billing.Visible = true;
                    mony.Visible = false;
                }
                else if (Hid_Peyments2.Value == "Cash")
                {
                    cach.Visible = true;
                    cradit.Visible = false;
                    billing.Visible = false;
                    mony.Visible = false;
                }
                else if (Hid_Peyments2.Value == "CreditCard")
                {
                    cach.Visible = false;
                    cradit.Visible = true;
                    billing.Visible = false;
                    mony.Visible = false;
                }
                else
                {
                    cach.Visible = false;
                    cradit.Visible = false;
                    billing.Visible = false;
                    mony.Visible = true;
                }

            }
        }
        public void Send_toEditbookss(string idbook)
        {
            Session["IDbook"] = idbook;
            Response.Redirect("/F_Edit_Booking.aspx");

        }

        protected void lbtn_a_Click(object sender, EventArgs e)
        {
            IDbook = ((HiddenField)grd_searchbook.Rows[((GridViewRow)((LinkButton)sender).NamingContainer).RowIndex].FindControl("Hid_bksid")).Value;
            Send_toEditbookss(IDbook);
        }

        protected void lbtn_d_Click(object sender, EventArgs e)
        {
            IDbook = ((HiddenField)grd_searchbook.Rows[((GridViewRow)((LinkButton)sender).NamingContainer).RowIndex].FindControl("Hid_bksid")).Value;
            Send_toEditbookss(IDbook);
        }

        protected void lbtn_b_Click(object sender, EventArgs e)
        {
            IDbook = ((HiddenField)grd_searchbook.Rows[((GridViewRow)((LinkButton)sender).NamingContainer).RowIndex].FindControl("Hid_bksid")).Value;
            Send_toEditbookss(IDbook);
        }

        protected void lbtn_c_Click(object sender, EventArgs e)
        {
            IDbook = ((HiddenField)grd_searchbook.Rows[((GridViewRow)((LinkButton)sender).NamingContainer).RowIndex].FindControl("Hid_bksid")).Value;
            Send_toEditbookss(IDbook);
        }

    }
}