﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SlimousineThailand
{
    public partial class F_ManageDiver : System.Web.UI.Page
    {
        static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
        private DataSet ds;
        SqlCommand sqlcomm = new SqlCommand();
        static bool modeUpdate = false;
        static string DriverIdEdit = "";
        static string Prefigg = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Clear();
                Set_GrdDri();
                //Set_grdmemall(txt_seach_mem.Text);
                if (modeUpdate == true)
                {
                    EditDri("");
                }
            }
        }

        public void Set_GrdDri()
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        // This will be your input parameter and its value
                        //sqlcomm.Parameters.AddWithValue("@paramValue", paramValue);

                        sqlcomm.CommandText = "spr_SetGrd_Driver";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            grd_dridetial.DataSource = ds.Tables[0];
                            grd_dridetial.DataBind();
                        }
                        else
                        {
                            grd_dridetial.DataSource = null;
                            grd_dridetial.DataBind();

                        }

                    }


                }
                //catch (SQLException ex)
                //{
                //    Console.WriteLine("SQL Error: " + ex.Message);
                //}
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
            }
        }
        private void InsertDri()
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        // This will be your input parameter and its value
                        sqlcomm.Parameters.AddWithValue("@Prefig", Prefigg);
                        sqlcomm.Parameters.AddWithValue("@Firstname", txt_fname_dri.Text);
                        sqlcomm.Parameters.AddWithValue("@Lastname", txt_lname_dri.Text);
                        sqlcomm.Parameters.AddWithValue("@Address", txt_address_dri.Text);
                        sqlcomm.Parameters.AddWithValue("@Phone", txt_tell_dri.Text);
                        sqlcomm.Parameters.AddWithValue("@Email", txt_email_dri.Text);


                        sqlcomm.CommandText = "spr_Inset_Driver";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);

                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Record Inserted Successfully')", true);


                    }


                }
                //catch (SQLException ex)
                //{
                //    Console.WriteLine("SQL Error: " + ex.Message);
                //}
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
                Clear();
                Set_GrdDri();
            }
        }
        public void EditDri(string DriverIdEdit)
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        // This will be your input parameter and its value
                        sqlcomm.Parameters.AddWithValue("@IDDriver", DriverIdEdit);

                        sqlcomm.CommandText = "spr_Edit_Driver";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            modeUpdate = true;

                            txt_fname_dri.Text = ds.Tables[0].Rows[0]["Firstname"].ToString();
                            txt_lname_dri.Text = ds.Tables[0].Rows[0]["Lastname"].ToString();
                            txt_tell_dri.Text = ds.Tables[0].Rows[0]["Phone"].ToString();
                            txt_email_dri.Text = ds.Tables[0].Rows[0]["Email"].ToString();
                            txt_address_dri.Text = ds.Tables[0].Rows[0]["Address"].ToString();

                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "showModal();", true);
                        }
                        else
                        {
                            modeUpdate = false;
                            this.Clear();
                        }

                    }


                }
                //catch (SQLException ex)
                //{
                //    Console.WriteLine("SQL Error: " + ex.Message);
                //}
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
            }
        }
        private void UpdateDri(string memberId)
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        // This will be your input parameter and its value
                        sqlcomm.Parameters.AddWithValue("@MemberID", memberId);
                        sqlcomm.Parameters.AddWithValue("@Prefig", Prefigg);
                        sqlcomm.Parameters.AddWithValue("@Firstname", txt_fname_dri.Text);
                        sqlcomm.Parameters.AddWithValue("@Lastname", txt_lname_dri.Text);
                        sqlcomm.Parameters.AddWithValue("@Phone", txt_tell_dri.Text);
                        sqlcomm.Parameters.AddWithValue("@Email", txt_email_dri.Text);
                        sqlcomm.Parameters.AddWithValue("@Address", txt_address_dri.Text);


                        sqlcomm.CommandText = "spr_Update_Driver";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);

                        Set_GrdDri();
                        modeUpdate = false;

                    }


                }
                //catch (SQLException ex)
                //{
                //    Console.WriteLine("SQL Error: " + ex.Message);
                //}
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
            }
        }

        private void Clear()
        {
            txt_fname_dri.Text = string.Empty;
            txt_lname_dri.Text = string.Empty;
            txt_email_dri.Text = string.Empty;
            txt_tell_dri.Text = string.Empty;
            txt_address_dri.Text = string.Empty;
            rdo_Mr_dri.Checked = true;
        }

        protected void btn_add_diver_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "showModal();", true);
        }

        protected void btn_edit_dri_Click(object sender, EventArgs e)
        {
            DriverIdEdit = ((HiddenField)grd_dridetial.Rows[((GridViewRow)((LinkButton)sender).NamingContainer).RowIndex].FindControl("HdriID")).Value;
            EditDri(DriverIdEdit);
        }

        protected void btn_delete_dri_Click(object sender, EventArgs e)
        {
            string id = ((HiddenField)grd_dridetial.Rows[((GridViewRow)((LinkButton)sender).NamingContainer).RowIndex].FindControl("HdriID")).Value;

            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        // This will be your input parameter and its value
                        sqlcomm.Parameters.AddWithValue("@IDMem", id);
                        sqlcomm.CommandText = "spr_Delete_Mem";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        Set_GrdDri();


                    }


                }

                catch (Exception ex)
                {
                    Console.WriteLine("Error: " + ex.Message);
                }
            }
        }

        protected void btn_Clear_dri_Click(object sender, EventArgs e)
        {
            Clear();
        }

        protected void btn_Save_dri_Click(object sender, EventArgs e)
        {
            
            if (rdo_Mr_dri.Checked==true)
            {
                Prefigg = "Mr";
            }
            else
            {
                Prefigg = "Ms";
            }
            if (txt_fname_dri.Text == "" && txt_lname_dri.Text == "" && txt_address_dri.Text == "" && txt_email_dri.Text == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('กรุณากรอกข้อมูล')", true);
                //ShowMessage("Record submitted successfully", MessageType.Warning);
                return;
            }
            else
            {
                

            }
            if (modeUpdate == false)
            {
                InsertDri();
            }
            else
            {
                UpdateDri(DriverIdEdit);
            }
        }
    }
}