﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SlimousineThailand
{
    public partial class F_BookingDate : System.Web.UI.Page
    {
        static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
        private DataSet ds;
        SqlCommand sqlcomm = new SqlCommand();
        static string Datenow = "";
        static string IDbook = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //Datenow = DateTime.Now.ToString("dd/MM/yyyy");
                Set_Grdseachdate();
            }
        }
        public void Set_Grdseachdate()
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        // This will be your input parameter and its value
                        //sqlcomm.Parameters.AddWithValue("@paramValue", paramValue);
                        //sqlcomm.Parameters.AddWithValue("@date", Datenow);

                        sqlcomm.CommandText = "spr_Seach_ByDate";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            grd_searchbookdate.DataSource = ds.Tables[0];
                            grd_searchbookdate.DataBind();
                        }
                        else
                        {
                            grd_searchbookdate.DataSource = null;
                            grd_searchbookdate.DataBind();

                        }

                    }


                }
                //catch (SQLException ex)
                //{
                //    Console.WriteLine("SQL Error: " + ex.Message);
                //}
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
            }
        }

        protected void grd_searchbookdate_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton statusa = (LinkButton)e.Row.FindControl("lbtn_edit_bkb");
                LinkButton statusb = (LinkButton)e.Row.FindControl("lbtn_confirm_bkb");
                LinkButton statusd = (LinkButton)e.Row.FindControl("lbtn_complete_bkb");
                LinkButton statusc = (LinkButton)e.Row.FindControl("lbtn_reject_bkb");

                Label cach = (Label)e.Row.FindControl("lbl_cach");
                Label cradit = (Label)e.Row.FindControl("lbl_cradit_card");
                Label billing = (Label)e.Row.FindControl("lbl_biling");
                Label mony = (Label)e.Row.FindControl("lbl_monytranfer");

                HiddenField hid_status = (e.Row.FindControl("Hid_bkdsid") as HiddenField);
                HiddenField Hid_Peyments = (e.Row.FindControl("Hid_Peyments") as HiddenField);

                if (hid_status.Value == "Pending")
                {

                    statusa.Visible = true;
                    statusb.Visible = false;
                    statusd.Visible = false;
                    statusc.Visible = false;
                }
                else if (hid_status.Value == "Comfirm")
                {
                    statusa.Visible = false;
                    statusb.Visible = true;
                    statusd.Visible = false;
                    statusc.Visible = false;
                }
                else if (hid_status.Value == "Complete")
                {
                    statusa.Visible = false;
                    statusb.Visible = false;
                    statusd.Visible = true;
                    statusc.Visible = false;
                }
                else
                {
                    statusa.Visible = false;
                    statusb.Visible = false;
                    statusd.Visible = false;
                    statusc.Visible = true;
                }


                if (Hid_Peyments.Value == "Billing")
                {

                    cach.Visible = false;
                    cradit.Visible = false;
                    billing.Visible = true;
                    mony.Visible = false;
                }
                else if (Hid_Peyments.Value == "Cash")
                {
                    cach.Visible = true;
                    cradit.Visible = false;
                    billing.Visible = false;
                    mony.Visible = false;
                }
                else if (Hid_Peyments.Value == "CreditCard")
                {
                    cach.Visible = false;
                    cradit.Visible = true;
                    billing.Visible = false;
                    mony.Visible = false;
                }
                else
                {
                    cach.Visible = false;
                    cradit.Visible = false;
                    billing.Visible = false;
                    mony.Visible = true;
                }


            }
        }
        public void Send_toEditbook(string idbook)
        {
            Session["IDbook"] = idbook;
            Response.Redirect("/F_Edit_Booking.aspx");
             
        }

        protected void lbtn_edit_bkb_Click(object sender, EventArgs e)
        {
            IDbook = ((HiddenField)grd_searchbookdate.Rows[((GridViewRow)((LinkButton)sender).NamingContainer).RowIndex].FindControl("Hid_bkid")).Value;
            Send_toEditbook(IDbook);
        }

        protected void lbtn_reject_bkb_Click(object sender, EventArgs e)
        {
            IDbook = ((HiddenField)grd_searchbookdate.Rows[((GridViewRow)((LinkButton)sender).NamingContainer).RowIndex].FindControl("Hid_bkid")).Value;
            Send_toEditbook(IDbook);
        }

        protected void lbtn_confirm_bkb_Click(object sender, EventArgs e)
        {
            IDbook = ((HiddenField)grd_searchbookdate.Rows[((GridViewRow)((LinkButton)sender).NamingContainer).RowIndex].FindControl("Hid_bkid")).Value;
            Send_toEditbook(IDbook);
        }

        protected void lbtn_complete_bkb_Click(object sender, EventArgs e)
        {
            IDbook = ((HiddenField)grd_searchbookdate.Rows[((GridViewRow)((LinkButton)sender).NamingContainer).RowIndex].FindControl("Hid_bkid")).Value;
            Send_toEditbook(IDbook);
        }
    }
}