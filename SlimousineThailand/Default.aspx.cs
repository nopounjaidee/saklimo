﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SlimousineThailand.CS;

namespace SlimousineThailand
{
    public partial class F_CrateBook : System.Web.UI.Page
    {
        SlimousineThailand.CS.Login loginemp = new CS.Login();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CS.Login cllogin = new CS.Login();
                cllogin.ClareLogin();
                
            }
        }

        protected void lbn_submit_login_Click(object sender, EventArgs e)
        {
            if (txt_Username_Login.Text != "" && txt_Pass_Login.Text != "")
            {
                if (loginemp.logins(txt_Username_Login.Text, txt_Pass_Login.Text))
                {
                    Response.Redirect("/F_BookingDate.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(),
                                    "err", "alert('" + "Login ผิดพลาด กรุณาตรวจสอบข้อมูลของท่าน" + "');", true);
                }

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(),
                                "err", "alert('" + "กรุณากรอก Uername/Password" + "');", true);
            }
           
        }

        
    }
}