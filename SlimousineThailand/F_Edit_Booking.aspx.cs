﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SlimousineThailand.CS;
using System.Globalization;
using System.Net.Mail;
using System.Net; //using Namespace System.Net.Mail 

namespace SlimousineThailand
{
    public partial class F_Edit_Booking : System.Web.UI.Page
    {
        static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
        private DataSet ds;
        static DataTable passendetail;
        SqlCommand sqlcomm = new SqlCommand();
        static bool Modeupdate = false;
        static string IDbook = "";
        static string rdocartype = "";
        static string slo = "";
        static string elo = "";
        static string statusbook = "";
        static string statuspay = "";
        static string fass = "";
        static string Glocals = "";
        static string Glocale = "";
        static string CheckPirce = "";
        static string IDDrivers = "";
        static string FnameDriv = "";
        static string LnameDriv = "";
        static string Sendto = "";
        static string CC = "";
        static string NameCustomer = "";
        static string lbErr = "";
        string smtpAddress = "smtp.gmail.com";
        static string  passmail = "";
        int portNumber = 587;
        bool enableSSL = true;
        CS.Login IDMem = new CS.Login();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                lbl_datebook_edb.Text = DateTime.Now.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
                txt_dayservicB_edb.Text = "0";
                txt_hourservicB_edb.Text = "0";
                txt_limoprice_edb.Text = "0";
                txt_fasttrackB_edb.Text = "0";
                txt_dayservicP_edb.Text = "0";
                txt_dayservicD_edb.Text = "0";
                txt_hourservicP_edb.Text = "0";
                txt_hourservicH_edb.Text = "0";
                txt_fasttrackP_edb.Text = "0";
                txt_fasttrackPack_edb.Text = "0";
                txt_balanceP_edb.Text = "0";
                txt_rentalP_edb.Text = "0";
                grd_passendetial_ed.DataSource = setdata();
                grd_passendetial_ed.DataBind();
                SetDDLGlocals();
                SetDDLGlocale();
                SetDDLAffiliate();
                IDbook = (string)Session["IDbook"];
                if (IDbook != "")
                {

                    SetEditBookupdate(IDbook);
                    Setdatetablepassen(IDbook);
                    Session["IDbook"] = "";
                    
                }
                
            }

            GetRDOCartype();
            if (CheckPirce != rdocartype)
            {
               SetLimoPrice(ddl_locals_edb.SelectedValue, ddl_locale_edb.SelectedValue, rdocartype);
            }
            pricess();
            lbl_trip_edb.Text = ddl_locals_edb.SelectedItem.ToString() + " . " + " TO " + " . " + ddl_locale_edb.SelectedItem.ToString();
        }
        private void InsetDatatable(string Prefig , string Fname, string Mname, string Lname, string Phon1, string Email)
        {
            passendetail.Rows.Add(Prefig ,Fname, Mname, Lname, Phon1, Email);
            grd_passendetial_ed.DataSource = passendetail;
            grd_passendetial_ed.DataBind();
        }
        public DataTable setdata()
        {

            passendetail = new DataTable();

            passendetail.Columns.Add("Prefig", typeof(string));
            passendetail.Columns.Add("Fname", typeof(string));
            passendetail.Columns.Add("Mname", typeof(string));
            passendetail.Columns.Add("Lname", typeof(string));
            passendetail.Columns.Add("Phon1", typeof(string));
            passendetail.Columns.Add("Email", typeof(string));


            return passendetail;


        }
        private void Setdatetablepassen(string IDBook)
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        // This will be your input parameter and its value
                        sqlcomm.Parameters.AddWithValue("@IDBooking", IDBook);

                        sqlcomm.CommandText = "spr_Edit_Passen";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                string fnamepass = "";
                                string mnamepass = "";
                                string lnamepass = "";
                                string phone1pass = "";
                                string emailpass = "";
                                string prefigpass = "";
                                prefigpass = ds.Tables[0].Rows[i]["Prefig"].ToString();
                                fnamepass = ds.Tables[0].Rows[i]["Firstname"].ToString();
                                mnamepass = ds.Tables[0].Rows[i]["Meddlename"].ToString();
                                lnamepass = ds.Tables[0].Rows[i]["LastName"].ToString();
                                emailpass = ds.Tables[0].Rows[i]["Email"].ToString();
                                phone1pass= ds.Tables[0].Rows[i]["Phone1"].ToString();
                                if (i == 0)
                                {
                                    Sendto = emailpass;
                                }
                                else if (i == 1)
                                {
                                    CC = emailpass;
                                }
                                else
                                {
                                    CC = CC + ";" + emailpass;
                                }

                                InsetDatatable(prefigpass, fnamepass, mnamepass, lnamepass, phone1pass, emailpass);
                            }
                        }
                        else
                        {
                        }

                    }


                }
                //catch (SQLException ex)
                //{
                //    Console.WriteLine("SQL Error: " + ex.Message);
                //}
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
            }

        }
        private void SetTextAffiliate(string IDAffiliate)
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        // This will be your input parameter and its value
                        sqlcomm.Parameters.AddWithValue("@IDAffiliate", IDAffiliate);

                        sqlcomm.CommandText = "spr_SetText_Affiliate";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {

                            txt_contectname_edb.Text = ds.Tables[0].Rows[0]["Name"].ToString();
                            txt_contectemail_edb.Text = ds.Tables[0].Rows[0]["Email"].ToString();
                            txt_contectphone_edb.Text = ds.Tables[0].Rows[0]["Phone"].ToString();
                        }
                        else
                        {
                        }

                    }


                }
                //catch (SQLException ex)
                //{
                //    Console.WriteLine("SQL Error: " + ex.Message);
                //}
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
            }
        }
        private void SetDDLAffiliate()
        {

            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        // This will be your input parameter and its value
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;

                        //sqlcomm.Parameters.AddWithValue("@id", "1");

                        sqlcomm.CommandText = "spr_SetDDL_Affiliate";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ddl_affiliate_edb.DataSource = ds.Tables[0];
                            ddl_affiliate_edb.DataTextField = "Name";
                            ddl_affiliate_edb.DataValueField = "AffiliateID";
                            ddl_affiliate_edb.DataBind();
                            
                        }
                        else
                        {
                            ddl_affiliate_edb.DataSource = null;
                            ddl_affiliate_edb.DataBind();
                        }

                    }
                }
                //catch (SQLException ex)
                //{
                //    Console.WriteLine("SQL Error: " + ex.Message);
                //}
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
                SetTextAffiliate(ddl_affiliate_edb.SelectedValue);
            }
        }
        private void SetDDLVehile(string IDCar)
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        // This will be your input parameter and its value
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;

                        sqlcomm.Parameters.AddWithValue("@IDCar", IDCar);

                        sqlcomm.CommandText = "spr_SetDDL_BranCar";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ddl_vehicle_edb.DataSource = ds.Tables[0];
                            ddl_vehicle_edb.DataTextField = "Brans";
                            ddl_vehicle_edb.DataValueField = "CarsID";
                            ddl_vehicle_edb.DataBind();
                        }
                        else
                        {
                            ddl_vehicle_edb.DataSource = null;
                            ddl_vehicle_edb.DataBind();
                        }

                    }
                }
                //catch (SQLException ex)
                //{
                //    Console.WriteLine("SQL Error: " + ex.Message);
                //}
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
            }

        }
        private void GetRDOCartype()
        {
            if (rdo_japan_edb.Checked)
            {
                rdocartype = "V1";
            }
            else if (rdo_normal_edb.Checked)
            {
                rdocartype = "V2";
            }
            else if (rdo_standard_edb.Checked)
            {
                rdocartype = "V3";
            }
            else if (rdo_topsedan_edb.Checked)
            {
                rdocartype = "V4";
            }
            else if (rdo_topvan_edb.Checked)
            {
                rdocartype = "V5";
            }
            else
            {
                rdocartype = "V6";
            }

            SetDDLVehile(rdocartype);
            txt_limoprice_edb.Focus();

        }
        private void SetLimoPrice(string slocal,string elocal,string veh)
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        // This will be your input parameter and its value
                        sqlcomm.Parameters.AddWithValue("@slocal", slocal);
                        sqlcomm.Parameters.AddWithValue("@elocal", elocal);
                        sqlcomm.Parameters.AddWithValue("@veh", veh);

                        sqlcomm.CommandText = "spr_SetLimoPrice";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {

                            txt_limoprice_edb.Text = ds.Tables[0].Rows[0]["Price"].ToString();
                            CheckPirce = veh;
                        }
                        else
                        {
                        }

                    }


                }
                //catch (SQLException ex)
                //{
                //    Console.WriteLine("SQL Error: " + ex.Message);
                //}
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
                
            }
        }
        private void SetDDLGlocals()
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        // This will be your input parameter and its value
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;

                        //sqlcomm.Parameters.AddWithValue("@id", "1");

                        sqlcomm.CommandText = "spr_Setddl_Createbook";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ddl_glocals_edb.DataSource = ds.Tables[0];
                            ddl_glocals_edb.DataTextField = "Name";
                            ddl_glocals_edb.DataValueField = "GlocationID";
                            ddl_glocals_edb.DataBind();
                        }
                        else
                        {
                            ddl_glocals_edb.DataSource = null;
                            ddl_glocals_edb.DataBind();
                        }

                    }
                }
                //catch (SQLException ex)
                //{
                //    Console.WriteLine("SQL Error: " + ex.Message);
                //}
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
                Glocals = ddl_glocals_edb.SelectedItem.ToString();
                SetDDLlocals(ddl_glocals_edb.SelectedValue);
            }
        }
        private void SetDDLGlocale()
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        // This will be your input parameter and its value
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;

                        //sqlcomm.Parameters.AddWithValue("@id", "1");

                        sqlcomm.CommandText = "spr_Setddl_Createbook";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ddl_glocale_edb.DataSource = ds.Tables[0];
                            ddl_glocale_edb.DataTextField = "Name";
                            ddl_glocale_edb.DataValueField = "GlocationID";
                            ddl_glocale_edb.DataBind();
                        }
                        else
                        {
                            ddl_glocale_edb.DataSource = null;
                            ddl_glocale_edb.DataBind();
                        }

                    }
                }
                //catch (SQLException ex)
                //{
                //    Console.WriteLine("SQL Error: " + ex.Message);
                //}
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
                Glocale = ddl_glocale_edb.SelectedItem.ToString();
                SetDDLlocale(ddl_glocale_edb.SelectedValue);
            }
        }
        private void GetIDLocal(string GIDLocal)
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        // This will be your input parameter and its value
                        sqlcomm.Parameters.AddWithValue("@IDlocal", GIDLocal);

                        sqlcomm.CommandText = "spr_SetGlocal_Book";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ddl_glocals_edb.SelectedValue = ds.Tables[0].Rows[0]["GlocationID"].ToString();
                            SetDDLlocals(ddl_glocals_edb.SelectedValue);
                            ddl_locals_edb.SelectedValue = GIDLocal;
                            //lbl_idbook_edb.Text = ds.Tables[0].Rows[0]["BookingID"].ToString();

                        }
                        else
                        {
                        }

                    }


                }
                //catch (SQLException ex)
                //{
                //    Console.WriteLine("SQL Error: " + ex.Message);
                //}
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
            }
        }
        private void GetIDLocale(string GIDLocale)
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        // This will be your input parameter and its value
                        sqlcomm.Parameters.AddWithValue("@IDlocal", GIDLocale);

                        sqlcomm.CommandText = "spr_SetGlocal_Book";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ddl_glocale_edb.SelectedValue = ds.Tables[0].Rows[0]["GlocationID"].ToString();
                            SetDDLlocale(ddl_glocale_edb.SelectedValue);
                            ddl_locale_edb.SelectedValue = GIDLocale;
                            //lbl_idbook_edb.Text = ds.Tables[0].Rows[0]["BookingID"].ToString();

                        }
                        else
                        {
                        }

                    }


                }
                //catch (SQLException ex)
                //{
                //    Console.WriteLine("SQL Error: " + ex.Message);
                //}
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
            }
        }
        private void GetIDDrive(string GIDDriv)
        {
            string prefig = "";
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        // This will be your input parameter and its value
                        sqlcomm.Parameters.AddWithValue("@GIDDriv", GIDDriv);

                        sqlcomm.CommandText = "spr_SetDriver_Book";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {

                            prefig = ds.Tables[0].Rows[0]["Prefig"].ToString();
                            FnameDriv = ds.Tables[0].Rows[0]["Firstname"].ToString();
                            LnameDriv = ds.Tables[0].Rows[0]["Lastname"].ToString();
                            txt_phone_driv_edb.Text = ds.Tables[0].Rows[0]["Phone"].ToString();
                            txt_Name_driv_edb.Text =prefig  + "   "  + FnameDriv  + "   "  +  LnameDriv;
                            IDDrivers = ds.Tables[0].Rows[0]["MemberID"].ToString();
                            txt_phone_driv_edb.Focus();

                        }
                        else
                        {
                        }

                    }


                }
                //catch (SQLException ex)
                //{
                //    Console.WriteLine("SQL Error: " + ex.Message);
                //}
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
            }
        }
        private void SetDDLlocals(string IDlocals)
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        // This will be your input parameter and its value
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;

                        //sqlcomm.Parameters.AddWithValue("@id", "1");
                        sqlcomm.Parameters.AddWithValue("@IDlocals", IDlocals);

                        sqlcomm.CommandText = "spr_SetDDL_locasCB";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ddl_locals_edb.DataSource = ds.Tables[0];
                            ddl_locals_edb.DataTextField = "Name";
                            ddl_locals_edb.DataValueField = "LocationID";
                            ddl_locals_edb.DataBind();
                        }
                        else
                        {
                            ddl_locals_edb.DataSource = null;
                            ddl_locals_edb.DataBind();
                        }

                    }
                }

                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
                slo = ddl_locals_edb.SelectedItem.ToString();
            }
        }
        private void SetDDLlocale(string IDlocale)
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        // This will be your input parameter and its value
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;

                        //sqlcomm.Parameters.AddWithValue("@id", "1");
                        sqlcomm.Parameters.AddWithValue("@IDlocals", IDlocale);

                        sqlcomm.CommandText = "spr_SetDDL_locasCB";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ddl_locale_edb.DataSource = ds.Tables[0];
                            ddl_locale_edb.DataTextField = "Name";
                            ddl_locale_edb.DataValueField = "LocationID";
                            ddl_locale_edb.DataBind();
                        }
                        else
                        {
                            ddl_locale_edb.DataSource = null;
                            ddl_locale_edb.DataBind();
                        }

                    }
                }

                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
                elo = ddl_locale_edb.SelectedItem.ToString();

            }
        }
        private void SetEditBookupdate(string idbook) 
        {
            
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        // This will be your input parameter and its value
                        sqlcomm.Parameters.AddWithValue("@IDBook", idbook);

                        sqlcomm.CommandText = "spr_Edit_Booking";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            Modeupdate = true;
                            btn_Delete_Book_edb.Visible = true;
                            DateTime yourDate = DateTime.Parse(ds.Tables[0].Rows[0]["Pickupdate"].ToString());
                            DateTime DateBook = DateTime.Parse(ds.Tables[0].Rows[0]["BookingDate"].ToString());
                            DateTime DateBookupdate = DateTime.Parse(ds.Tables[0].Rows[0]["BookingUpdate"].ToString());

                            lbl_idbook_edb.Text = ds.Tables[0].Rows[0]["BookingID"].ToString();
                            lbl_datebook_edb.Text = DateBook.ToString("dd/MM/yyyy  HH:mm:ss");
                            lbl_booksatus_edb.Text = ds.Tables[0].Rows[0]["Bookingstatus"].ToString();
                            txt_dropoff_local_edb.Text = ds.Tables[0].Rows[0]["DropOfflocation"].ToString();
                            string GIDLocal = ds.Tables[0].Rows[0]["StatlocationID"].ToString();
                            GetIDLocal(GIDLocal);
                            txt_pickup_local_edb.Text = ds.Tables[0].Rows[0]["Pickuplocation"].ToString();
                            lbl_paybook_edb.Text = ds.Tables[0].Rows[0]["Paymentstatus"].ToString();                            
                            string GIDLocale = ds.Tables[0].Rows[0]["EndlocationID"].ToString();
                            GetIDLocale(GIDLocale);
                            rdocartype = ds.Tables[0].Rows[0]["VehicleID"].ToString();
                            NameCustomer = ds.Tables[0].Rows[0]["Prefig"].ToString() + " . " + ds.Tables[0].Rows[0]["Firstname"].ToString() + " " + ds.Tables[0].Rows[0]["Meddlename"].ToString() + " " + ds.Tables[0].Rows[0]["LastName"].ToString();

                            if (rdocartype == "V1")
                            {
                                rdo_japan_edb.Checked = true;
                            }
                            else if (rdocartype == "V2")
                            {
                                rdo_normal_edb.Checked = true;
                            }
                            else if (rdocartype == "V3")
                            {
                                rdo_standard_edb.Checked = true;
                            }
                            else if (rdocartype == "V4")
                            {
                                rdo_topsedan_edb.Checked = true;
                            }
                            else if (rdocartype == "V5")
                            {
                                rdo_topvan_edb.Checked = true;
                            }
                            else
                            {
                                rdo_vipvan_edb.Checked = true;
                            }
                            CheckPirce = rdocartype;
                            GetRDOCartype();
                            ddl_vehicle_edb.SelectedValue = ds.Tables[0].Rows[0]["CarsID"].ToString();
                            txt_servicremack_edb.Text = ds.Tables[0].Rows[0]["Serviceremark"].ToString();
                            txt_limoprice_edb.Text = ds.Tables[0].Rows[0]["SlimouPrice"].ToString();
                            txt_dayservicP_edb.Text = ds.Tables[0].Rows[0]["ByDateServicePrice"].ToString();
                            txt_dayservicD_edb.Text = ds.Tables[0].Rows[0]["ByDateServiceDay"].ToString();
                            txt_hourservicP_edb.Text = ds.Tables[0].Rows[0]["ByHourServicePrice"].ToString();
                            txt_hourservicH_edb.Text = ds.Tables[0].Rows[0]["ByHourServiceHour"].ToString();
                            txt_fasttrackP_edb.Text = ds.Tables[0].Rows[0]["Fasttrackprice"].ToString();
                            txt_fasttrackPack_edb.Text = ds.Tables[0].Rows[0]["Fasttrackpax"].ToString();
                            txt_fasttrackremack_edb.Text = ds.Tables[0].Rows[0]["Fasttrackremark"].ToString();
                            txt_carno_edb.Text = ds.Tables[0].Rows[0]["CarNo"].ToString();
                            ddl_affiliate_edb.SelectedValue = ds.Tables[0].Rows[0]["AffiliateID"].ToString();
                            txt_flightno_edb.Text = ds.Tables[0].Rows[0]["FlightNo"].ToString();
                            txt_pickup_date_edb.Text = yourDate.ToString("yyyy/MM/dd");
                            txt_pickup_time_edb.Text = ds.Tables[0].Rows[0]["Pickuptime"].ToString();
                            txt_pickup_detail_edb.Text = ds.Tables[0].Rows[0]["Pickupdetail"].ToString();
                            txt_dropoff_detail_edb.Text = ds.Tables[0].Rows[0]["DropOffdetail"].ToString();
                            txt_contectname_edb.Text = ds.Tables[0].Rows[0]["Contactname"].ToString();
                            txt_contectemail_edb.Text = ds.Tables[0].Rows[0]["Contactemail"].ToString();
                            txt_contectphone_edb.Text = ds.Tables[0].Rows[0]["Contactphone"].ToString();
                            txt_contextdetail_edb.Text = ds.Tables[0].Rows[0]["Contactdetail"].ToString();
                            if (ds.Tables[0].Rows[0]["DrivID"].ToString() != "")
                            {
                                GetIDDrive(ds.Tables[0].Rows[0]["DrivID"].ToString());
                            }
                            ddl_satus_pay_edb.SelectedValue = ds.Tables[0].Rows[0]["Paymentstatus"].ToString();
                            ddl_status_book_edb.SelectedValue = ds.Tables[0].Rows[0]["Bookingstatus"].ToString();
                            lbl_bookupdate_edb.Text = DateBookupdate.ToString("dd/MM/yyyy  HH:mm:ss");

                            if (ddl_status_book_edb.SelectedValue == "Comfirm")
                            {
                                btn_Delete_Book_edb.Visible = true;
                                btn_SendMail_book_edb.Visible = true;
                            }
                            else if (ddl_status_book_edb.SelectedValue == "Cancel")
                            {
                                btn_Delete_Book_edb.Visible = true;
                            }
                            else if (ddl_status_book_edb.SelectedValue == "Pending")
                            {
                                btn_Delete_Book_edb.Visible = true;
                            }
                            else
                            {
                                btn_Delete_Book_edb.Visible = false;
                                btn_SendMail_book_edb.Visible = true;
                            }

                            
                            //txt_Email_driv_edb.Text = ds.Tables[0].Rows[0]["Address"].ToString();
                            //txt_phone_driv_edb.Text = ds.Tables[0].Rows[0]["Address"].ToString();
                            //txt_address_dri.Text = ds.Tables[0].Rows[0]["Address"].ToString();
                            //txt_address_dri.Text = ds.Tables[0].Rows[0]["Address"].ToString();

                        }
                        else
                        {
                            Modeupdate = false;
                        }

                    }


                }
                //catch (SQLException ex)
                //{
                //    Console.WriteLine("SQL Error: " + ex.Message);
                //}
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
            }
        }
        protected void bnt_add_drive_ed_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "showModal();", true);
        }
        protected void btn_add_passen_edb_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "showModal1();", true);
        }
        protected void ddl_glocals_edb_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetDDLlocals(ddl_glocals_edb.SelectedValue);
            lbl_trip_edb.Text = ddl_locals_edb.SelectedItem.ToString() + " . " + " TO " + " . " + ddl_locale_edb.SelectedItem.ToString();
            rdo_vipvan_edb.Focus();
        }
        protected void ddl_glocale_edb_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetDDLlocale(ddl_glocale_edb.SelectedValue);
            lbl_trip_edb.Text = ddl_locals_edb.SelectedItem.ToString() + " . " + " TO " + " . " + ddl_locale_edb.SelectedItem.ToString();
            rdo_vipvan_edb.Focus();
        }
        protected void ddl_affiliate_edb_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetTextAffiliate(ddl_affiliate_edb.SelectedValue);
            txt_contectname_edb.Focus();
        }
        private void pricess()
        {
            if (txt_dayservicP_edb.Text == "")
            {
                txt_dayservicP_edb.Text = "0";
            }
            else if (txt_dayservicD_edb.Text == "")
            {
                txt_dayservicD_edb.Text = "0";
            }
            else if (txt_hourservicP_edb.Text == "")
            {
                txt_hourservicP_edb.Text = "0";
            }
            else if (txt_hourservicH_edb.Text == "")
            {
                txt_hourservicH_edb.Text = "0";
            }
            else if (txt_fasttrackP_edb.Text == "")
            {
                txt_fasttrackP_edb.Text = "0";
            }
            else if (txt_fasttrackPack_edb.Text == "")
            {
                txt_fasttrackPack_edb.Text = "0";
            }
            else if (txt_rentalP_edb.Text == "")
            {
                txt_rentalP_edb.Text = "0";
            }
            decimal dayservicprice = decimal.Parse(txt_dayservicP_edb.Text);
            decimal dayservicDay = decimal.Parse(txt_dayservicD_edb.Text);
            txt_dayservicB_edb.Text = Convert.ToString(dayservicprice * dayservicDay);

            decimal hourservicprice = decimal.Parse(txt_hourservicP_edb.Text);
            decimal hourservicHou = decimal.Parse(txt_hourservicH_edb.Text);
            txt_hourservicB_edb.Text = Convert.ToString(hourservicprice * hourservicHou);

            decimal fasttrackrice = decimal.Parse(txt_fasttrackP_edb.Text);
            decimal fasttrackPack = decimal.Parse(txt_fasttrackPack_edb.Text);
            txt_fasttrackB_edb.Text = Convert.ToString(fasttrackrice * fasttrackPack);

            decimal limoprice = decimal.Parse(txt_limoprice_edb.Text);
            decimal dayservicB = decimal.Parse(txt_dayservicB_edb.Text);
            decimal hourservicB = decimal.Parse(txt_hourservicB_edb.Text);
            decimal fasttrackB = decimal.Parse(txt_fasttrackB_edb.Text);
            txt_totalB_edb.Text = Convert.ToString(limoprice + dayservicB + hourservicB + fasttrackB);

            txt_balanceP_edb.Text = txt_totalB_edb.Text.ToString();
            decimal balanceP = decimal.Parse(txt_balanceP_edb.Text);
            decimal rentalP = decimal.Parse(txt_rentalP_edb.Text);
            txt_ENDPrice_edb.Text = Convert.ToString(balanceP - rentalP);


        }
        protected void txt_limoprice_edb_TextChanged(object sender, EventArgs e)
        {
            
        }
        protected void txt_dayservicP_edb_TextChanged(object sender, EventArgs e)
        {
           
        }
        protected void ddl_locals_edb_SelectedIndexChanged(object sender, EventArgs e)
        {
            pricess();
            rdo_vipvan_edb.Focus();
        }
        protected void ddl_locale_edb_SelectedIndexChanged(object sender, EventArgs e)
        {
            pricess();
            rdo_vipvan_edb.Focus();
        }
        private void InsertBooking()
        {
            string pf = "";
            string fn = "";
            string mn = "";
            string ln = "";
            for (int i = 0; i < passendetail.Rows.Count; i++)
            {
                pf = passendetail.Rows[i]["Prefig"].ToString();
                fn = passendetail.Rows[i]["Fname"].ToString();
                mn = passendetail.Rows[i]["Mname"].ToString();
                ln = passendetail.Rows[i]["Lname"].ToString();
                i = 20;

            }
            
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        sqlcomm.Parameters.AddWithValue("@AffiliateID", ddl_affiliate_edb.SelectedValue);
                        sqlcomm.Parameters.AddWithValue("@CarsID", ddl_vehicle_edb.SelectedValue);
                        sqlcomm.Parameters.AddWithValue("@VehicleID", rdocartype);
                        sqlcomm.Parameters.AddWithValue("@DrivID", IDDrivers);
                        sqlcomm.Parameters.AddWithValue("@MemberID", IDMem.getMemID());
                        sqlcomm.Parameters.AddWithValue("@FlightNo", txt_flightno_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@Pickupdate", txt_pickup_date_edb.Text.ToString());
                        sqlcomm.Parameters.AddWithValue("@Pickuptime", txt_pickup_time_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@StatlocationID", ddl_locals_edb.SelectedValue);
                        sqlcomm.Parameters.AddWithValue("@Statlocation", slo);
                        sqlcomm.Parameters.AddWithValue("@Pickupdetail", txt_pickup_detail_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@Pickuplocation", txt_pickup_local_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@EndlocationID", ddl_locale_edb.SelectedValue);
                        sqlcomm.Parameters.AddWithValue("@Endlocation", elo);
                        sqlcomm.Parameters.AddWithValue("@DropOffdetail", txt_dropoff_detail_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@DropOfflocation", txt_dropoff_local_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@Contactname", txt_contectname_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@Contactemail", txt_contectemail_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@Contactphone", txt_contectphone_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@Contactdetail", txt_contextdetail_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@Bookingstatus", statusbook );
                        sqlcomm.Parameters.AddWithValue("@Paymentstatus", statuspay );
                        sqlcomm.Parameters.AddWithValue("@SlimouPrice", txt_limoprice_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@ByDateServicePrice", txt_dayservicP_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@ByHourServicePrice", txt_hourservicP_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@ByDateServiceDay", txt_dayservicD_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@ByHourServiceHour", txt_hourservicH_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@Serviceremark", txt_servicremack_edb.Text );
                        sqlcomm.Parameters.AddWithValue("@Fasttrack", fass);
                        sqlcomm.Parameters.AddWithValue("@Fasttrackprice", txt_fasttrackP_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@Fasttrackpax", txt_fasttrackPack_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@Fasttrackremark", txt_fasttrackremack_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@Grandtotal", txt_totalB_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@Rentalcar", txt_rentalP_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@Net", txt_balanceP_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@CarNo", txt_carno_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@Prefig", pf);
                        sqlcomm.Parameters.AddWithValue("@Firstname", fn);
                        sqlcomm.Parameters.AddWithValue("@Meddlename", mn);
                        sqlcomm.Parameters.AddWithValue("@LastName", ln);


                        sqlcomm.CommandText = "spr_Insert_Bookings";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            string Idbook = "";
                            Idbook = ds.Tables[0].Rows[0]["BookingID"].ToString();

                            for (int i = 0; i < passendetail.Rows.Count; i++)
                            {
                                string Prefigpas = "";
                                string Fnames = "";
                                string Mname = "";
                                string Lname = "";
                                string Phon1 = "";
                                string Email = "";
                                Prefigpas = passendetail.Rows[i]["Prefig"].ToString();
                                Fnames = passendetail.Rows[i]["Fname"].ToString();
                                Mname = passendetail.Rows[i]["Mname"].ToString();
                                Lname = passendetail.Rows[i]["Lname"].ToString();
                                Phon1 = passendetail.Rows[i]["Phon1"].ToString();
                                Email = passendetail.Rows[i]["Email"].ToString();
                                InsertPassen( Idbook,Prefigpas, Fnames, Mname, Lname, Email, Phon1);

                            }

                        }
                        else
                        {
                        }
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Record Inserted Successfully')", true);


                    }


                }
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }

                Modeupdate = false;
                Response.Redirect("/F_BookingDate.aspx");

            }
        }
        private void InsertPassen(string IDbook, string Prefigpas, string fname, string mname, string lname, string email, string phone1)
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        sqlcomm.Parameters.AddWithValue("@BookingID", IDbook);
                        sqlcomm.Parameters.AddWithValue("@Firstname", fname);
                        sqlcomm.Parameters.AddWithValue("@Meddlename", mname);
                        sqlcomm.Parameters.AddWithValue("@LastName", lname);
                        sqlcomm.Parameters.AddWithValue("@Email", email);
                        sqlcomm.Parameters.AddWithValue("@Phone1", phone1);
                        sqlcomm.Parameters.AddWithValue("@Prefig", Prefigpas);


                        sqlcomm.CommandText = "spr_Insert_Passen";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);

                        //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Record Inserted Successfully')", true);


                    }


                }
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
            }
        }
        private void UpdateBooking()
        {
            string pfs = "";
            string fns = "";
            string mns = "";
            string lns = "";
            if (passendetail.Rows.Count > 0)
            {
                pfs = passendetail.Rows[0]["Prefig"].ToString();
                fns = passendetail.Rows[0]["Fname"].ToString();
                mns = passendetail.Rows[0]["Mname"].ToString();
                lns = passendetail.Rows[0]["Lname"].ToString();
            }
            else
            {
                pfs = "";
                fns = "";
                mns = "";
                lns = "";
            }
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        sqlcomm.Parameters.AddWithValue("@IDbook", IDbook);
                        sqlcomm.Parameters.AddWithValue("@AffiliateID", ddl_affiliate_edb.SelectedValue);
                        sqlcomm.Parameters.AddWithValue("@CarsID", ddl_vehicle_edb.SelectedValue);
                        sqlcomm.Parameters.AddWithValue("@VehicleID", rdocartype);
                        sqlcomm.Parameters.AddWithValue("@DrivID", IDDrivers);
                        sqlcomm.Parameters.AddWithValue("@MemberID", IDMem.getMemID());
                        sqlcomm.Parameters.AddWithValue("@FlightNo", txt_flightno_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@Pickupdate", txt_pickup_date_edb.Text.ToString());
                        sqlcomm.Parameters.AddWithValue("@Pickuptime", txt_pickup_time_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@StatlocationID", ddl_locals_edb.SelectedValue);
                        sqlcomm.Parameters.AddWithValue("@Statlocation", slo);
                        sqlcomm.Parameters.AddWithValue("@Pickupdetail", txt_pickup_detail_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@Pickuplocation", txt_pickup_local_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@EndlocationID", ddl_locale_edb.SelectedValue);
                        sqlcomm.Parameters.AddWithValue("@Endlocation", elo);
                        sqlcomm.Parameters.AddWithValue("@DropOffdetail", txt_dropoff_detail_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@DropOfflocation", txt_dropoff_local_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@Contactname", txt_contectname_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@Contactemail", txt_contectemail_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@Contactphone", txt_contectphone_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@Contactdetail", txt_contextdetail_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@Bookingstatus", statusbook);
                        sqlcomm.Parameters.AddWithValue("@Paymentstatus", statuspay);
                        sqlcomm.Parameters.AddWithValue("@SlimouPrice", txt_limoprice_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@ByDateServicePrice", txt_dayservicP_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@ByHourServicePrice", txt_hourservicP_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@ByDateServiceDay", txt_dayservicD_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@ByHourServiceHour", txt_hourservicH_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@Serviceremark", txt_servicremack_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@Fasttrack", fass);
                        sqlcomm.Parameters.AddWithValue("@Fasttrackprice", txt_fasttrackP_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@Fasttrackpax", txt_fasttrackPack_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@Fasttrackremark", txt_fasttrackremack_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@Grandtotal", txt_totalB_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@Rentalcar", txt_rentalP_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@Net", txt_balanceP_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@CarNo", txt_carno_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@Prefig", pfs);
                        sqlcomm.Parameters.AddWithValue("@Firstname", fns);
                        sqlcomm.Parameters.AddWithValue("@Meddlename", mns);
                        sqlcomm.Parameters.AddWithValue("@LastName", lns);


                        sqlcomm.CommandText = "spr_Update_Booking";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        DeletePassen(IDbook);
                            for (int i = 0; i < passendetail.Rows.Count; i++)
                            {
                                string Preifigg = "";
                                string Fnames = "";
                                string Mname = "";
                                string Lname = "";
                                string Phon1 = "";
                                string Email = "";
                                Preifigg = passendetail.Rows[i]["Prefig"].ToString();
                                Fnames = passendetail.Rows[i]["Fname"].ToString();
                                Mname = passendetail.Rows[i]["Mname"].ToString();
                                Lname = passendetail.Rows[i]["Lname"].ToString();
                                Phon1 = passendetail.Rows[i]["Phon1"].ToString();
                                Email = passendetail.Rows[i]["Email"].ToString();
                                InsertPassen(IDbook, Preifigg, Fnames, Mname, Lname, Email, Phon1);

                            }


                    }


                }
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
                Modeupdate = false;
                IDbook = "";
                Session["IDbook"] = "";
                Response.Redirect("/F_BookingDate.aspx");

            }
        }
        private void DeleteBook(string IDBooking)
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        // This will be your input parameter and its value
                        sqlcomm.Parameters.AddWithValue("@IDBooking", IDBooking);

                        sqlcomm.CommandText = "spr_Delete_Book";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        

                    }


                }
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
            }
        }
        private void DeletePassen(string IDBooking)
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        // This will be your input parameter and its value
                        sqlcomm.Parameters.AddWithValue("@IDBooking", IDBooking);

                        sqlcomm.CommandText = "spr_Delete_Passen";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);


                    }


                }
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
            }
        }
        protected void bnt_search_driv_edb_Click(object sender, EventArgs e)
        {
            if (txt_Shrech_EMP_MS.Text == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please fill this out.')", true);
                //ShowMessage("Record submitted successfully", MessageType.Warning);
                return;
            }
            else
            {
                Set_GridDriver(txt_Shrech_EMP_MS.Text.ToString());
                txt_phone_driv_edb.Focus();
            }
        }
        public void Set_GridDriver(string memberid)
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        // This will be your input parameter and its value
                        sqlcomm.Parameters.AddWithValue("@GIDDriv", memberid);

                        sqlcomm.CommandText = "spr_SetDriver_Book";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            grd_Driver_ed.DataSource = ds.Tables[0];
                            grd_Driver_ed.DataBind();
                        }
                        else
                        {
                            grd_Driver_ed.DataSource = null;
                            grd_Driver_ed.DataBind();

                        }

                    }


                }
                //catch (SQLException ex)
                //{
                //    Console.WriteLine("SQL Error: " + ex.Message);
                //}
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
            }
        }

        protected void btn_Select_Driver_ed_Click(object sender, EventArgs e)
        {
            IDDrivers = ((HiddenField)grd_Driver_ed.Rows[((GridViewRow)((LinkButton)sender).NamingContainer).RowIndex].FindControl("HIDDriv")).Value;
            GetIDDrive(IDDrivers);
            Set_GridDriver(IDDrivers);
        }

        protected void btn_touwgub_edb_Click(object sender, EventArgs e)
        {
            txt_rentalP_edb.Focus();
        }

        protected void btn_cancle_edb_Click(object sender, EventArgs e)
        {
            txt_fname_pas_edb.Text = string.Empty;
            txt_mname_pas_edb.Text = string.Empty;
            txt_lname_pas_edb.Text = string.Empty;
            txt_phone_pas_edb.Text = string.Empty;
            txt_email_pas_edb.Text = string.Empty;
            txt_rentalP_edb.Focus();
        }

        protected void btn_submit_addpassen_edb_Click(object sender, EventArgs e)
        {
            string fname = txt_fname_pas_edb.Text;
            string mname = txt_mname_pas_edb.Text;
            string lname = txt_lname_pas_edb.Text;
            string phone1 = txt_phone_pas_edb.Text;
            string email = txt_email_pas_edb.Text;
            string rdoprefig = "";
            if (rdo_Mr_edb.Checked == true)
            {
                rdoprefig = "Mr";
            }
            else
            {
                rdoprefig = "Ms";
            }
            if (fname == "" && mname == "" && lname == "" && phone1 == ""  && email == "")
            {

            }
            else
            {
                InsetDatatable(rdoprefig, fname, mname, lname, phone1, email);
            }
            txt_fname_pas_edb.Text = string.Empty;
            txt_mname_pas_edb.Text = string.Empty;
            txt_lname_pas_edb.Text = string.Empty;
            txt_phone_pas_edb.Text = string.Empty;
            txt_email_pas_edb.Text = string.Empty;

        }

        protected void btn_Delete_Book_edb_Click(object sender, EventArgs e)
        {
            DeleteBook(IDbook);
            DeletePassen(IDbook);
            Response.Redirect("/F_BookingDate.aspx");
        }

        protected void btn_Confirm_Book_edb_Click(object sender, EventArgs e)
        {
            slo = ddl_glocals_edb.SelectedItem.ToString();
            elo = ddl_glocale_edb.SelectedItem.ToString();
            statusbook = ddl_status_book_edb.SelectedItem.ToString();
            statuspay = ddl_satus_pay_edb.SelectedItem.ToString();
            fass = ddl_fasttrack_edb.SelectedItem.ToString();
            if (Modeupdate == true)
            {
                if (txt_flightno_edb.Text == "")
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please fill this out.')", true);
                }
                else if (txt_pickup_date_edb.Text == "")
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please fill this out.')", true);
                }
                else
                {
                    //ลบข้อมูลpassen ก่อนเเล้วค่อย update และ Add passen เข้าไปใหม่
                    UpdateBooking();
                }
                
            }
            else
            {
                if (txt_flightno_edb.Text == "")
                {
                     ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please fill this out.')", true);
                }
                else if (txt_pickup_date_edb.Text == "")
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please fill this out.')", true);
                }
                else
                {
                    InsertBooking();
                }
                
            }
        }

        protected void btn_delete_passen_ed_Click(object sender, EventArgs e)
        {
            if (passendetail.Rows.Count >= 1)
            {
                passendetail.Rows[((GridViewRow)((LinkButton)sender).NamingContainer).RowIndex].Delete();

            }
            grd_passendetial_ed.DataSource = passendetail;
            grd_passendetial_ed.DataBind();
        }

        private void InsertDri()
        {
            string prefigs = "";
            if (rdo_Mr_driedb.Checked == true)
            {
                prefigs = "Mr";
            }
            else
            {
                prefigs = "Ms";
            }
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        // This will be your input parameter and its value
                        sqlcomm.Parameters.AddWithValue("@Prefig", prefigs);
                        sqlcomm.Parameters.AddWithValue("@Firstname", txt_fname_div_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@Lastname", txt_lname_div_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@Address", "");
                        sqlcomm.Parameters.AddWithValue("@Phone", txt_tell_div_edb.Text);
                        sqlcomm.Parameters.AddWithValue("@Email", "");


                        sqlcomm.CommandText = "spr_Inset_Driver";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                    }


                }
                //catch (SQLException ex)
                //{
                //    Console.WriteLine("SQL Error: " + ex.Message);
                //}
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
            }
        }

        protected void btn_Clear_emp_Click(object sender, EventArgs e)
        {
            txt_fname_div_edb.Text = string.Empty;
            txt_lname_div_edb.Text = string.Empty;
            txt_tell_div_edb.Text = string.Empty;
        }

        protected void btn_Save_emp_Click(object sender, EventArgs e)
        {
            InsertDri();
        }

        protected void btn_SendMail_book_edb_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "showModalpassmail();", true);
            
        }

        // เมื่อคลิกปุ่ม
        protected void btn_sendmail_edb_Click(object sender, EventArgs e)
        {
            if (txt_sendto_edb.Text != "")//ตรวจสอบค่าว่างของ txtTo
            {
                /** 
                ตัดคำด้วยเครื่องหมาย ";" semi-colon(เซมิโคลอน)
                ทั้ง txtTo และ txtCC เพื่อหาจำนวน Email ที่จะส่ง
                **/
                string[] tmpTO = txt_sendto_edb.Text.Split(';');
                string[] tmpCC = txt_cc_edb.Text.Split(';');
                string Mass = "<strong> Dear  </strong>" + NameCustomer + "<br/>" + " We would like to confirm your car transfer service by " + lbl_nameaffi.Text.ToString() + " as following <br/>"; Mass += " " + "<table><p><br /><strong>BOOKING ID : </strong> " + lbl_idbook_edb.Text.ToString() + "<br /> <strong>Flight Detail :</strong> " + txt_flightno_edb.Text.ToString() + "<br /><strong>Pick up date :</strong>" + txt_pickup_date_edb.Text.ToString() + "<br /><strong>Pick up time :</strong>" + txt_pickup_time_edb.Text.ToString() + "<br /><strong>Pick up location : </strong>" + txt_pickup_local_edb.Text.ToString() + "<br /><strong> Drop off location : </strong>" + txt_dropoff_local_edb.Text.ToString() + "<br /><strong> Balance price : </strong> " + txt_balanceP_edb.Text.ToString() + " " + "Baht" + "</p><br /><p>As the number of passenger and luggage is essential require so would you please contact us back to reconfirm and/or<br />to revise the detail via Phone: +66 839878054, +66 99 286 8467<br />Email address : info@saklimousinethailand.com<br />Best Regards,<br />Reservation Agent Saklimousinethailand</p><p>Saklimousinethailand - World Wide Saklimousinethailand and Premium Service</p><p>*******************************************</p><p>Concierge Line 24 hours service via +66 839878054, +66 99 286 8467</p><p>E-mail : info@saklimousinethailand.com</p><p>Website : www.saklimousinethailand.com</p></table>";
                    

                //สร้าง Aray ไว้เก็บชื่อ Email ทั้งจดหมายที่จะส่ง และ CC -*-
                string[] Cc = new string[tmpCC.Length];
                string[] sendTo = new string[tmpTO.Length];

                //ลูปเก็บ Email ที่จะส่งเก็บไว้ใน Array ซะ
                for (int i = 0; i < tmpTO.Length; i++)
                {
                    if (tmpTO[i] != "")
                    {
                        sendTo[i] = tmpTO[i];
                    }
                }

                for (int j = 0; j < tmpCC.Length; j++)
                {
                    if (tmpCC[j] != "")
                    {
                        Cc[j] = tmpCC[j];
                    }
                }

                // ส่ง Email ด้วย Method sendEmail ที่สร้างขั้นมา มี อาร์กิวเมนต์  4 ตัว ตามชื่อเรยจ้า
                // มี อาร์กิวเมนต์ที่เป็น array สองตัวคือ sendTo และ Cc นอกนั้นเป็น string   
                sendEmail(txt_Subject_edb.Text, sendTo, Cc, Mass);

            }

        }
        private void sendEmail(string inSubject, string[] inSendTo, string[] inCC, string inMsg)
        {
            MailMessage mailMsg = new MailMessage();//สร้าง Object MailMessage
            // กำหนด Email ของผู้ส่ง
            string femail = "info@saklimousinethailand.com";
            string passemail = passmail;
            mailMsg.From = new MailAddress(femail);

            //เช็คจำนวน Email มีค่ามากกว่า 0 หรือไม่
            if (inSendTo.Length > 0)
            {
                //ลูป Add Email ผู้รับ
                for (int j = 0; j < inSendTo.Length; j++)
                {
                    //เช็คว่า Array ตัวนี้ไม่ได้มีค่าเป็น null 
                    if (inSendTo[j] != null)
                    {
                        //Add Email ผู้รับเข้าปาาย
                        mailMsg.To.Add(new MailAddress(inSendTo[j].ToString().Trim()));
                    }
                }
            }

            //เช็คจำนวน Email มีค่ามากกว่า 0 หรือไม่(CC)
            if (inCC.Length > 0)
            {
                ////ลูป Add Email CC
                for (int i = 0; i < inCC.Length; i++)
                {
                    if (inCC[i] != null)
                    {
                        //Add Email CC เข้าปาาย
                        mailMsg.CC.Add(new MailAddress(inCC[i].ToString().Trim()));
                    }
                }
            }

            //ชื่อ Email
            mailMsg.Subject = inSubject;
            //Encode ชื่อจดหมายให้อักขระเป็นไทย
            mailMsg.SubjectEncoding = System.Text.Encoding.GetEncoding("Windows-874");

            //เนื้อหาของ Email
            mailMsg.Body = inMsg;

            mailMsg.IsBodyHtml = true;
            //Encode เนื้อหา Email ให้อักขระเป็นไทย
            //mailMsg.BodyEncoding = System.Text.Encoding.GetEncoding("Windows-874");

            //กำหนด SMTP
            SmtpClient smtp = new SmtpClient();
            //ผมใส่ SMTP ของ Maxnet ครับใครใช้ Internet ของค่ายไหนก็ใส่ให้ถูกต้องครับ
            //สามารถหา SMTP ของผู้ให้บริการอินเทอร์เน็ตได้จาก Google เลยจ้าาา
            smtp.Credentials = new NetworkCredential(femail, passemail);
            smtp.EnableSsl = enableSSL;
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            //smtp.Host = "smtp.tttmaxnet.com";

            //เช็ค Error
            try
            {
                //ไม่มี Error ก็ให้ส่งเมลล์ไปเรย และแสดงข้อความว่า "Email ถูกส่งแล้ว" ใน Label
                smtp.Send(mailMsg);
                lbErr = "Email ถูกส่งไปแล้ว";
                Updatepassemail();
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Email ถูกส่งไปแล้ว')", true);

            }
            catch (Exception ex)
            {
                //ถ้ามี Error หรือข้อผิดพลาดก็ให้แสดงออกมาหน่อย ใน Label
                lbErr = ex.Message;
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('PASSWORD ไม่ถูกต้อง')", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "showModalpassmail();", true);
            }
        }

        protected void btn_passemail_edb_Click(object sender, EventArgs e)
        {
            passmail = txt_passmail_edb.Text.ToString();
            txt_sendto_edb.Text = Sendto;
            txt_cc_edb.Text = CC;
            txt_Subject_edb.Text = "Saklimousinethailand : Confirmation Email Ref." + lbl_idbook_edb.Text.ToString();
            lbl_namecus_edb.Text = NameCustomer;
            lbl_nameaffi.Text = ddl_affiliate_edb.SelectedItem.ToString();
            lbl_idbooking_edb.Text = lbl_idbook_edb.Text.ToString();
            lbl_flight_edb.Text = txt_flightno_edb.Text.ToString();
            lbl_pickup_edb.Text = txt_pickup_date_edb.Text.ToString();
            lbl_pickuptime_edb.Text = txt_pickup_time_edb.Text.ToString();
            lbl_pickuplocation_edb.Text = txt_pickup_local_edb.Text.ToString();
            lbl_dropoff_edb.Text = txt_dropoff_local_edb.Text.ToString();
            lbl_balance_price_edb.Text = txt_balanceP_edb.Text.ToString() + " " + "Baht";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "showModalmail();", true);
        }
        private void Updatepassemail()
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        sqlcomm.Parameters.AddWithValue("@Passemail", passmail);
                        sqlcomm.CommandText = "spr_Update_passemail";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                    }


                }
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }

            }
        }

       
        
    }
}