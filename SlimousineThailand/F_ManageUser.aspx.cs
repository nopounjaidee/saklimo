﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SlimousineThailand
{
    public partial class F_ManageUser : System.Web.UI.Page
    {
        static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
        private DataSet ds;
        SqlCommand sqlcomm = new SqlCommand();
        static bool modeUpdate = false;
        static string MemberIdEdit = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Clear();
                Set_GrdMem();
                //Set_grdmemall(txt_seach_mem.Text);
                if (modeUpdate == true)
                {
                    EditMem("");
                }
            }
        }

        protected void btn_add_user_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "showModal();", true);
        }
        public void Set_GrdMem ()
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        // This will be your input parameter and its value
                        //sqlcomm.Parameters.AddWithValue("@paramValue", paramValue);

                        sqlcomm.CommandText = "spr_SetGrd_Mem";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            grd_memdetial.DataSource = ds.Tables[0];
                            grd_memdetial.DataBind();
                        }
                        else
                        {
                            grd_memdetial.DataSource = null;
                            grd_memdetial.DataBind();

                        }

                    }


                }
                //catch (SQLException ex)
                //{
                //    Console.WriteLine("SQL Error: " + ex.Message);
                //}
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
            }
        }
        private void InsertMem()
        {
            string  rdoprefig = "";
            if (rdo_Mr_Emp.Checked == true)
            {
                rdoprefig = "Mr";
            }
            else
            {
                rdoprefig = "Ms";
            }
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        // This will be your input parameter and its value
                        sqlcomm.Parameters.AddWithValue("@Prefig", rdoprefig);
                        sqlcomm.Parameters.AddWithValue("@Username", txt_username_Emp.Text);
                        sqlcomm.Parameters.AddWithValue("@Passwords", txt_password_Emp.Text);
                        sqlcomm.Parameters.AddWithValue("@Firstname", txt_fname_Emp.Text);
                        sqlcomm.Parameters.AddWithValue("@Lastname", txt_lname_Emp.Text);
                        sqlcomm.Parameters.AddWithValue("@Address", txt_Address_emp.Text);
                        sqlcomm.Parameters.AddWithValue("@Phone", txt_tell_Emp.Text);
                        sqlcomm.Parameters.AddWithValue("@Email", txt_email_Emp.Text);


                        sqlcomm.CommandText = "spr_Inset_Mem";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);

                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Record Inserted Successfully')", true);


                    }


                }
                //catch (SQLException ex)
                //{
                //    Console.WriteLine("SQL Error: " + ex.Message);
                //}
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
                Clear();
                Set_GrdMem();
            }
        }
        public void EditMem(string MemberId)
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        // This will be your input parameter and its value
                        sqlcomm.Parameters.AddWithValue("@IDMem", MemberId);

                        sqlcomm.CommandText = "spr_Edit_Mem";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            modeUpdate = true;

                            txt_username_Emp.Text = ds.Tables[0].Rows[0]["Username"].ToString();
                            txt_password_Emp.Text = ds.Tables[0].Rows[0]["Passwords"].ToString();
                            txt_fname_Emp.Text = ds.Tables[0].Rows[0]["Firstname"].ToString();
                            txt_lname_Emp.Text = ds.Tables[0].Rows[0]["Lastname"].ToString();
                            txt_tell_Emp.Text = ds.Tables[0].Rows[0]["Phone"].ToString();
                            txt_email_Emp.Text = ds.Tables[0].Rows[0]["Email"].ToString();
                            txt_Address_emp.Text = ds.Tables[0].Rows[0]["Address"].ToString();

                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "showModal();", true);
                        }
                        else
                        {
                            modeUpdate = false;
                            this.Clear();
                        }

                    }


                }
                //catch (SQLException ex)
                //{
                //    Console.WriteLine("SQL Error: " + ex.Message);
                //}
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
            }
        }
        private void UpdateMem(string memberId)
        {
            string rdoprefigu = "";
            if (rdo_Mr_Emp.Checked == true)
            {
                rdoprefigu = "Mr";
            }
            else
            {
                rdoprefigu = "Ms";
            }
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        // This will be your input parameter and its value
                        sqlcomm.Parameters.AddWithValue("@MemberID", memberId);
                        sqlcomm.Parameters.AddWithValue("@Prefig", rdoprefigu);
                        sqlcomm.Parameters.AddWithValue("@Username", txt_username_Emp.Text);
                        sqlcomm.Parameters.AddWithValue("@Passwords", txt_password_Emp.Text);
                        sqlcomm.Parameters.AddWithValue("@Firstname", txt_fname_Emp.Text);
                        sqlcomm.Parameters.AddWithValue("@Lastname", txt_lname_Emp.Text);
                        sqlcomm.Parameters.AddWithValue("@Phone", txt_tell_Emp.Text);
                        sqlcomm.Parameters.AddWithValue("@Email", txt_email_Emp.Text);
                        sqlcomm.Parameters.AddWithValue("@Address", txt_Address_emp.Text);


                        sqlcomm.CommandText = "spr_Update_Mem";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);

                        Set_GrdMem();
                        modeUpdate = false;

                    }


                }
                //catch (SQLException ex)
                //{
                //    Console.WriteLine("SQL Error: " + ex.Message);
                //}
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
            }
        }
        private void Clear() 
        {
            txt_username_Emp.Text = string.Empty;
            txt_password_Emp.Text = string.Empty;
            txt_fname_Emp.Text = string.Empty;
            txt_lname_Emp.Text = string.Empty;
            txt_tell_Emp.Text = string.Empty;
            txt_email_Emp.Text = string.Empty;
            txt_Address_emp.Text = string.Empty;
            txt_description_Emp.Text = string.Empty;
            rdo_Mr_Emp.Checked = true;
        }

        protected void btn_Clear_emp_Click(object sender, EventArgs e)
        {
            Clear();
        }

        protected void btn_Save_emp_Click(object sender, EventArgs e)
        {
            int amount = 0;
            if (txt_username_Emp.Text == "" && txt_password_Emp.Text == "" && txt_fname_Emp.Text == "" && txt_lname_Emp.Text == "" && txt_email_Emp.Text == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('กรุณากรอกข้อมูล')", true);
                //ShowMessage("Record submitted successfully", MessageType.Warning);
                return;
            }
            else
            {
                

            }
            if (modeUpdate == false)
            {
                InsertMem();
            }
            else
            {
                UpdateMem(MemberIdEdit);
            }
            //Set_GrdMem();
        }

        protected void btn_delete_mem_Click(object sender, EventArgs e)
        {
            string id = ((HiddenField)grd_memdetial.Rows[((GridViewRow)((LinkButton)sender).NamingContainer).RowIndex].FindControl("HdnID")).Value;

            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        // This will be your input parameter and its value
                        sqlcomm.Parameters.AddWithValue("@IDMem", id);
                        sqlcomm.CommandText = "spr_Delete_Mem";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        Set_GrdMem();


                    }


                }

                catch (Exception ex)
                {
                    Console.WriteLine("Error: " + ex.Message);
                }
            }
        }

        protected void btn_edit_mem_Click(object sender, EventArgs e)
        {
            MemberIdEdit = ((HiddenField)grd_memdetial.Rows[((GridViewRow)((LinkButton)sender).NamingContainer).RowIndex].FindControl("HdnID")).Value;
            EditMem(MemberIdEdit);
        }

    }
}