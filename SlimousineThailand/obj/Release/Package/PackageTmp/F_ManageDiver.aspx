﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Slimousine.Master" AutoEventWireup="true" CodeBehind="F_ManageDiver.aspx.cs" Inherits="SlimousineThailand.F_ManageDiver" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">MANAGE USER</h4>
        </div>
    </div>


    <!-- Bootstrap Modals -->
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <%--<h4 class="header-title m-t-0 m-b-30">Bootstrap Modals (default)</h4>--%>

                <p class="text-muted m-b-15 font-13">
                    Add DiverSaklimousineThailand
                </p>
               <div id="db" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title">Diver</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">First Name</label>
                                            <%--<input type="text" class="form-control" id="field-1" placeholder="John">--%>
                                            <asp:TextBox ID="txt_fname_dri" runat="server" class="form-control" placeholder="First Name"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="field-2" class="control-label">Last Name</label>
                                            <asp:TextBox ID="txt_lname_dri" runat="server" class="form-control" placeholder="Last Name"></asp:TextBox>
                                            <%--<input type="text" class="form-control" id="field-2" placeholder="Password">--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="field-3" class="control-label">Address</label>
                                            <asp:TextBox ID="txt_address_dri" runat="server" class="form-control" placeholder="Address"></asp:TextBox>
                                            <%--<input type="text" class="form-control" id="field-3" placeholder="สถานที่ติดต่อ-อาคาร 1 ชั้น 4 คณะเทคโนโลยีสารสนเทศ">--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="field-4" class="control-label">Phone</label>
                                            <asp:TextBox ID="txt_tell_dri" runat="server" class="form-control" placeholder="Phone"></asp:TextBox>
                                            <%--<input type="text" class="form-control" id="field-4" placeholder="เบอร์มือถือ">--%>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="field-5" class="control-label">E-Mail</label>
                                            <asp:TextBox ID="txt_email_dri" runat="server" class="form-control" placeholder="E-Mail  @nortbankok.com"></asp:TextBox>
                                            <%--<input type="text" class="form-control" id="field-5" placeholder="E-Mail  @nortbankok.com">--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group no-margin">
                                            <label for="field-7" class="control-label">Descriptions</label>
                                            <asp:TextBox ID="txt_description_div" runat="server" TextMode="MultiLine" class="form-control autogrow" placeholder="Descriptions"></asp:TextBox>
                                            <%--<textarea class="form-control autogrow" id="field-7" placeholder="Write something about yourself" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;"></textarea>--%>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <%--<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">ยกเลิก</button>--%>
                                <asp:Button ID="btn_Clear_dri" OnClick="btn_Clear_dri_Click" runat="server" Text="ยกเลิก" CssClass="btn btn-default" />
                                <asp:Button ID="btn_Save_dri" OnClick="btn_Save_dri_Click" runat="server" Text="บันทึก" CssClass="btn btn-info" />
                                <%--<asp:button type="button" class="btn btn-info waves-effect waves-light" ID="btn_Save_div" >Save changes</asp:button>--%>
                                <%--<asp:Button ID="btn_Save_div" runat="server" Text="Save changes" CssClass="btn btn-info waves-effect waves-light" />--%>
                                <%--<button type="button" class="btn btn-info waves-effect waves-light">Save changes</button>--%>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.modal -->

                <div class="button-list">
                    <%--<button type="button" class="btn btn-custom dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Settings <span class="m-l-5"><i class="fa fa-cog"></i></span></button>--%>
                    <asp:LinkButton ID="btn_add_diver" OnClick="btn_add_diver_Click" class="btn btn-custom dropdown-toggle waves-effect waves-light" runat="server" >AddDiver<span class="m-l-5"><i class="fa fa-cog"></i></span></asp:LinkButton>
                </div>
            </div>
        </div>
    </div>


    <!-- Custom Modals -->
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">


                <h4 class="header-title m-t-0 m-b-30">DIVER</h4>

             <%--   <p class="text-muted m-b-15 font-13">
                    Examples of custom modals.
                </p>--%>

                <div class="form-group">
                     <div class="form-group">
                    <asp:GridView ID="grd_dridetial" runat="server" CssClass="table table-striped table-bordered dfdf" GridLines="None" EmptyDataText="ไม่มีรายการ"
                                AutoGenerateColumns="False" >
                                        <Columns>
                                    <asp:TemplateField >
                                        <HeaderStyle Width="80px"  />
                                        <ItemTemplate>
                                            <asp:HiddenField ID="HdriID" runat="server" Value='<%# Eval("MemberID") %>'></asp:HiddenField>
                                            <%--<asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" Width="20px"
                                                ImageUrl="~/img/Edit.png"  Text="Edit"  />&nbsp;
                                            <asp:ImageButton ID="imgDelete" runat="server" CssClass="col-md-2" CausesValidation="False"  Width="20px"
                                                OnClientClick="return confirm('ยืนยันการลบข้อมูล');" CommandName="Delete"
                                                ImageUrl="~/img/delete.png" Text="Delete"   />--%>
                                            <asp:LinkButton ID="btn_edit_dri" CausesValidation="False"  OnClick="btn_edit_dri_Click"   CssClass="btn btn-icon waves-effect waves-light btn-warning m-b-5" runat="server"  ><span class="fa fa-wrench"></span></asp:LinkButton>
                                            <asp:LinkButton ID="btn_delete_dri" CausesValidation="False" OnClick="btn_delete_dri_Click"   OnClientClick="return confirm('ยืนยันการลบข้อมูล');" CssClass="btn btn-icon waves-effect waves-light btn-danger m-b-5" runat="server"  ><span class="fa fa-remove"></span></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle Width="80px" />
                                    </asp:TemplateField>
                                    <%--<asp:BoundField DataField="memid" HeaderText="ID" />--%>
                                    <asp:BoundField DataField="MemberID" HeaderText="ID" />
                                    <asp:BoundField DataField="Firstname" HeaderText="Firstname" />
                                    <asp:BoundField DataField="Lastname" HeaderText="Lastname" />
                                    <asp:BoundField DataField="Phone" HeaderText="PhoneNumer" />
                                    <asp:BoundField DataField="Email" HeaderText="E-Mail" />
                                </Columns>
                                <RowStyle CssClass="cursor-pointer" />


                                    </asp:GridView>
                </div>
                </div>

            </div>
        </div>
        <!-- end col -->
    </div>
    <!-- End row -->

    <!-- Right Sidebar -->
    <div class="side-bar right-bar">
        <a href="javascript:void(0);" class="right-bar-toggle">
            <i class="zmdi zmdi-close-circle-o"></i>
        </a>
        <h4 class="">T`H`E`M`E`L`O`C`K`.`C`O`M`</h4>
        <div class="notification-list nicescroll">
            <ul class="list-group list-no-border user-list">
                <li class="list-group-item">
                    <a href="#" class="user-list-item">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-2.jpg" alt="">
                        </div>
                        <div class="user-desc">
                            <span class="name">Michael Zenaty</span>
                            <span class="desc">There are new settings available</span>
                            <span class="time">2 hours ago</span>
                        </div>
                    </a>
                </li>
                <li class="list-group-item">
                    <a href="#" class="user-list-item">
                        <div class="icon bg-info">
                            <i class="zmdi zmdi-account"></i>
                        </div>
                        <div class="user-desc">
                            <span class="name">New Signup</span>
                            <span class="desc">There are new settings available</span>
                            <span class="time">5 hours ago</span>
                        </div>
                    </a>
                </li>
                <li class="list-group-item">
                    <a href="#" class="user-list-item">
                        <div class="icon bg-pink">
                            <i class="zmdi zmdi-comment"></i>
                        </div>
                        <div class="user-desc">
                            <span class="name">New Message received</span>
                            <span class="desc">There are new settings available</span>
                            <span class="time">1 day ago</span>
                        </div>
                    </a>
                </li>
                <li class="list-group-item active">
                    <a href="#" class="user-list-item">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-3.jpg" alt="">
                        </div>
                        <div class="user-desc">
                            <span class="name">James Anderson</span>
                            <span class="desc">There are new settings available</span>
                            <span class="time">2 days ago</span>
                        </div>
                    </a>
                </li>
                <li class="list-group-item active">
                    <a href="#" class="user-list-item">
                        <div class="icon bg-warning">
                            <i class="zmdi zmdi-settings"></i>
                        </div>
                        <div class="user-desc">
                            <span class="name">Settings</span>
                            <span class="desc">There are new settings available</span>
                            <span class="time">1 day ago</span>
                        </div>
                    </a>
                </li>

            </ul>
        </div>
    </div>
    <!-- /Right-bar -->
</asp:Content>
