﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Slimousine.Master" AutoEventWireup="true" CodeBehind="F_CrateBooking.aspx.cs" Inherits="SlimousineThailand.F_CrateBooking" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
    <div class="row">
        <div class="col-sm-12">

            <h4 class="page-title">Booking wizard</h4>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-md-4">
                        <div class="control-group">
                            <fieldset>
                                <legend>Origin :</legend>
                                <div class="input-group has-success m-t-10" style="width: 100%">
                                    <asp:DropDownList ID="ddl_glocals_cb" OnSelectedIndexChanged="ddl_glocals_cb_SelectedIndexChanged" runat="server" class="form-control" AutoPostBack="True">
                                       <%-- <asp:ListItem>Suvannapum </asp:ListItem>
                                        <asp:ListItem>Donmuang</asp:ListItem>
                                        <asp:ListItem>Bankkok Zon</asp:ListItem>
                                        <asp:ListItem>Out Off Bankkok</asp:ListItem>--%>
                                    </asp:DropDownList>
                                </div>
                                <div class="input-group has-success m-t-10 " style="width: 100%">
                                    <asp:DropDownList ID="ddl_locals_cb" runat="server" class="form-control">
                                        <%--<asp:ListItem>Suvannapum</asp:ListItem>
                                        <asp:ListItem>Donmuang</asp:ListItem>
                                        <asp:ListItem>Bankkok Zon</asp:ListItem>
                                        <asp:ListItem>Out Off Bankkok</asp:ListItem>--%>
                                    </asp:DropDownList>
                                </div>
                            </fieldset>
                        </div>
                        <div class="control-group m-t-30">
                            <div class="control-group">
                                <fieldset>
                                    <legend>Destination :</legend>
                                    <div class="input-group has-error m-t-10" style="width: 100%">
                                        <asp:DropDownList ID="ddl_Glocae_cb" OnSelectedIndexChanged="ddl_Glocae_cb_SelectedIndexChanged" runat="server" class="form-control" AutoPostBack="True">
                                        <%--    <asp:ListItem>Suvannapum</asp:ListItem>
                                            <asp:ListItem>Donmuang</asp:ListItem>
                                            <asp:ListItem>Bankkok Zon</asp:ListItem>
                                            <asp:ListItem>Out Off Bankkok</asp:ListItem>--%>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="input-group has-error m-t-10 " style="width: 100%">
                                        <asp:DropDownList ID="ddl_locae_cb" runat="server" class="form-control">
                                         <%--   <asp:ListItem>Suvannapum</asp:ListItem>
                                            <asp:ListItem>Donmuang</asp:ListItem>
                                            <asp:ListItem>Bankkok Zon</asp:ListItem>
                                            <asp:ListItem>Out Off Bankkok</asp:ListItem>--%>
                                        </asp:DropDownList>
                                    </div>
                                </fieldset>
                            </div>

                        </div>
                        <div class="control-group m-t-30">
                            <div class="control-group" >
                                <div class="controls">
                                    <label>CarType</label>

                                    <div class="radio radio-success">
                                        <asp:RadioButton ID="rdo_japan_cb" runat="server" Text="Japanese Luxury Sedan (Camry)" GroupName="Cartype" Checked="true" />
                                    </div>

                                    <div class="radio radio-success">
                                        <asp:RadioButton ID="rdo_normal_cb" runat="server" Text="Normal Van" GroupName="Cartype" />
                                    </div>

                                    <div class="radio radio-success">
                                        <asp:RadioButton ID="rdo_standard_cb" runat="server" Text="Standard Luxury Sedan" GroupName="Cartype" />
                                    </div>

                                    <div class="radio radio-success">
                                        <asp:RadioButton ID="rdo_topsededan_cb" runat="server" Text="Top Luxury Sedan" GroupName="Cartype" />
                                    </div>

                                    <div class="radio radio-success">
                                        <asp:RadioButton ID="rdo_topvan_cb" runat="server" Text="Top Luxury Van (Benz Vito)" GroupName="Cartype" />
                                    </div>

                                    <div class="radio radio-success">
                                        <asp:RadioButton ID="rdo_vipvan_cb" runat="server" Text="Vip Van" GroupName="Cartype" />
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-2">

                        <div class="control-group" id="positionGroup">
                            <div class="control-group">
                                <div class="controls">
                                    <label>Pickup Date</label>
                                    <%--<input id="Text5" type="text" placeholder="swing, linear" class="input-mini form-control" value="swing" />--%>
                                    <asp:TextBox ID="txt_pickup_date_cb" runat="server" class="input-mini form-control datepicker-autoclose"></asp:TextBox>

                                </div>
                            </div>
                            <div class="control-group">
                                <div class="controls">
                                    <label>Pickup Time</label>
                                    <%--<input id="Text5" type="text" placeholder="swing, linear" class="input-mini form-control" value="swing" />--%>
                                    <asp:TextBox ID="txt_pickup_time_cb" runat="server"  class="input-mini form-control timepicker2 col-md-2"></asp:TextBox>
                                    <i class="glyphicon glyphicon-time"></i>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="controls">
                                    <label>.</label>
                                    <%--<input id="Text5" type="text" placeholder="swing, linear" class="input-mini form-control" value="swing" />--%>
                                </div>
                            </div>
                            <div class="controls">
                                <label>Service</label>

                                <div class="radio radio-custom">
                                    <%--<input type="radio" name="positions" id="radio5" value="toast-top-right" checked />--%>
                                    <asp:RadioButton ID="rdo_1_cb" runat="server" Text="Standard Service" GroupName="Service" Checked="true" />
                                </div>
                                <div class="radio radio-custom">
                                    <%--<input type="radio" name="positions" id="radio6" value="toast-bottom-right" />--%>
                                    <asp:RadioButton ID="rdo_2_cb" runat="server" Text="VIP Service Upon Arrival (With golf cart)" GroupName="Service" />
                                </div>

                                <div class="radio radio-custom">
                                    <%--<input type="radio" name="positions" id="radio7" value="toast-bottom-left" />--%>
                                    <asp:RadioButton ID="rdo_3_cb" runat="server" Text="VIP Service For Departure (With lounge)" GroupName="Service" />
                                </div>

                                <div class="radio radio-custom">
                                    <asp:RadioButton ID="rdo_4_cb" runat="server" Text="Visa on Arrival" GroupName="Service" />
                                </div>

                                <div class="radio radio-custom">
                                    <asp:RadioButton ID="rdo_5_cb" runat="server" Text="VIP Service" GroupName="Service" />
                                </div>

                                <div class="radio radio-custom">
                                    <asp:RadioButton ID="rdo_6_cb" runat="server" Text="Transit Service (Standard)" GroupName="Service" />
                                </div>

                                <div class="radio radio-custom">
                                    <asp:RadioButton ID="rdo_7_cb" runat="server" Text="Transit Service (VIP)" GroupName="Service" />
                                </div>

                                <%--<div class="radio radio-custom">
                                    <asp:RadioButton ID="RadioButton8" runat="server" Text="VIP Service"  />
                                    
                                    <label for="radio12">
                                        Bottom Center
                                    </label>
                                </div>--%>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-6">
                        <div class="row">
                            <div class="control-group">
                                <div class="controls">
                                    <label for="showEasing">Passenger  : </label>

                                </div>
                                <div class="controls col-md-4">
                                    <label for="showEasing">FirstName</label>
                                    <%--<input id="showEasing" type="text" placeholder="swing, linear" class="input-mini form-control" value="swing" />--%>
                                    <asp:TextBox ID="txt_Fname_cb" runat="server" class="input-mini form-control"></asp:TextBox>
                                </div>
                                <div class="controls col-md-3">
                                    <label for="showEasing">Middle</label>
                                    <asp:TextBox ID="txt_Middlename_cb" runat="server" class="input-mini form-control" ></asp:TextBox>
                                </div>
                                <div class="controls col-md-5">
                                    <label for="showEasing">LastName</label>
                                    <asp:TextBox ID="txt_Lastname_cb" runat="server" class="input-mini form-control" ></asp:TextBox>
                                </div>
                            </div>
                        </div>
                         <div class="row">
                            <div class="control-group">
                                <div class="controls">
                                   .

                                </div>
                                <div class="controls col-md-5">
                                    <label for="showEasing">Phone1</label>
                                    <%--<input id="showEasing" type="text" placeholder="swing, linear" class="input-mini form-control" value="swing" />--%>
                                    <asp:TextBox ID="txt_phone1_cb" runat="server" class="input-mini form-control"></asp:TextBox>
                                </div>
                                <div class="controls col-md-5">
                                    <label for="showEasing">Phone2</label>
                                    <asp:TextBox ID="txt_phone2_cb" runat="server" class="input-mini form-control" ></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="control-group">
                                <div class="controls">
                                   .

                                </div>
                                <div class="controls col-md-8">
                                    <label for="showEasing">E-mail</label>
                                    <%--<input id="showEasing" type="text" placeholder="swing, linear" class="input-mini form-control" value="swing" />--%>
                                    <asp:TextBox ID="txt_email_cb" runat="server" class="input-mini form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row m-t-30">
                            <div class="col-md-8">
                            </div>
                            <div class="col-md-2">
                                <%--<button type="button" class="btn btn-primary waves-effect waves-light" id="Button2">Add Passenger</button>--%>
                                <asp:LinkButton ID="btn_Addpassen_cb" runat="server" OnClick="btn_Addpassen_cb_Click" type="button" class="btn waves-effect waves-light btn-primary"><i class="zmdi zmdi-account-add"></i> Add Up</asp:LinkButton>
                            </div>
                        </div>

                        <div class="row >
                             <div class="control-group" >
                                 .
                             </div>
                            <div class="controls ">
                                   <asp:GridView ID="grd_passendetial" runat="server" CssClass="table table-striped table-bordered dfdf" GridLines="None" EmptyDataText="ไม่มีรายการ"
                                AutoGenerateColumns="False" >
                                        <Columns>
                                    <asp:TemplateField >
                                        <HeaderStyle Width="80px"  />
                                        <ItemTemplate>
                                            <%--<asp:HiddenField ID="HAddpassenID" runat="server" Value='<%# Eval("ID") %>'></asp:HiddenField>--%>
                                            <%--<asp:HiddenField ID="Hid_opid" runat="server" Value='<%# Eval("subopenID") %>'></asp:HiddenField>--%>
                                            <%--<asp:LinkButton ID="btn_edit_passen_cb" CausesValidation="False"     CssClass="btn btn-icon waves-effect waves-light btn-warning m-b-5" runat="server"  ><span class="fa fa-wrench"></span></asp:LinkButton>--%>
                                            <asp:LinkButton ID="btn_delete_passen_cb" CausesValidation="False" OnClick="btn_delete_passen_cb_Click"    OnClientClick="return confirm('ยืนยันการลบข้อมูล');" CssClass="btn btn-icon waves-effect waves-light btn-danger m-b-5" runat="server"  ><span class="fa fa-remove"></span></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle Width="80px" />
                                    </asp:TemplateField>
                                    <%--<asp:BoundField DataField="memid" HeaderText="ID" />--%>
                                    <asp:BoundField DataField="Fname" HeaderText="FirstName" />
                                    <asp:BoundField DataField="Mname" HeaderText="MiddleName" />
                                    <asp:BoundField DataField="Lname" HeaderText="LastName" />
                                    <asp:BoundField DataField="Phon1" HeaderText="PhoneNumber" />
                                </Columns>
                                <RowStyle CssClass="cursor-pointer" />


                                    </asp:GridView>
                                </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->

                <div class="row m-t-30">
                    <div class="col-md-12">
                        <%--<button type="button" class="btn btn-primary waves-effect waves-light" id="showtoast">Submit</button>
                        <button type="button" class="btn btn-danger waves-effect waves-light" id="cleartoasts">Clear</button>--%>
                        <asp:LinkButton ID="btn_Booking_cb" runat="server" OnClick="btn_Booking_cb_Click" class="btn btn-primary waves-effect waves-light">Booking</asp:LinkButton>
                        <asp:LinkButton ID="btn_clear_cb" runat="server" OnClick="btn_clear_cb_Click" class="btn btn-danger waves-effect waves-light">Clear</asp:LinkButton>
                    </div>
                </div>

                <div class="row m-t-30">
                    <div class="col-md-12">
                        <pre id='toastrOptions'></pre>
                    </div>
                </div>
            </div>
        </div>
        <!-- end col -->
    </div>
    <!-- end row -->
</asp:Content>
