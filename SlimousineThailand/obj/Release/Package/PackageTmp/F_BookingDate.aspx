﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Slimousine.Master" AutoEventWireup="true" CodeBehind="F_BookingDate.aspx.cs" Inherits="SlimousineThailand.F_BookingDate" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">

            <h4 class="page-title">BOOKING LIST</h4>
        </div>
    </div>

   
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">

                <%--<h4 class="header-title m-t-0 m-b-30">มคอ</h4>--%>
                <%--<asp:Label ID="lbl_HQTF" runat="server" Text="มคอ" class="header-title m-t-0 m-b-30"></asp:Label>--%>



                <asp:GridView ID="grd_searchbookdate" OnRowDataBound="grd_searchbookdate_RowDataBound"  runat="server" GridLines="None" EmptyDataText="ไม่มีรายการ"
                    AutoGenerateColumns="False" CssClass=" table table-striped table-bordered dfdf">

                    <Columns>

                        <asp:BoundField DataField="BookingID" HeaderText="BOOKING ID" />
                        <asp:BoundField DataField="Statlocation" HeaderText="Pickup" />
                        <asp:BoundField DataField="Endlocation" HeaderText="DropOff" />
                        <asp:BoundField DataField="FlightNo" HeaderText="Flight NO" />
                        <asp:BoundField DataField="Pickupdate" HeaderText="Pickup Date" DataFormatString="{0:dd/MM/yyyy}" />
                        <asp:BoundField DataField="Pickuptime" HeaderText="Pickup Time" />

                        <asp:TemplateField HeaderText="Booking Status">
                            <HeaderStyle/>
                            <ItemTemplate>
                                <asp:HiddenField ID="Hid_bkid" runat="server" Value='<%# Eval("BookingID") %>'></asp:HiddenField>
                                <asp:HiddenField ID="Hid_bkdsid" runat="server" Value='<%# Eval("Bookingstatus") %>'></asp:HiddenField>
                                <%--<asp:HiddenField ID="hid_status" runat="server" Value='<%# Eval("status") %>'></asp:HiddenField>--%>
                                <asp:LinkButton ID="lbtn_edit_bkb" OnClick="lbtn_edit_bkb_Click" CausesValidation="False" CssClass="btn btn-icon waves-effect waves-light btn-warning m-b-5" runat="server"><span class="fa fa-wrench"> Wait.</span></asp:LinkButton>
                                <asp:LinkButton ID="lbtn_reject_bkb" OnClick="lbtn_reject_bkb_Click" CausesValidation="False" CssClass="btn btn-icon waves-effect btn-danger m-b-5" runat="server"><i class="fa fa-wrench" style="width: auto;margin-right: 10px;"></i> Reject</span></asp:LinkButton>
                                <asp:LinkButton ID="lbtn_confirm_bkb" OnClick="lbtn_confirm_bkb_Click" CausesValidation="False" OnClientClick="return confirm('Comfirm & SendMail ?');" CssClass="btn btn-icon waves-effect waves-light btn-primary m-b-5" runat="server"><i class="fa fa-spin fa-spinner" style="width: auto;margin-right: 10px;"></i> Comfirm</span></asp:LinkButton>
                                <asp:LinkButton ID="lbtn_complete_bkb" OnClick="lbtn_complete_bkb_Click" CausesValidation="False" CssClass="btn btn-icon waves-effect waves-light btn-success m-b-5" runat="server"><span class="zmdi zmdi-check-circle"> Complete</span></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Contactname" HeaderText="Contect Name" />
                        <asp:BoundField DataField="Contactphone" HeaderText="Contect Phone" />

                        <%--<asp:BoundField DataField="status" HeaderText="status" />--%>
                    </Columns>
                    <RowStyle CssClass="cursor-pointer" />

                </asp:GridView>
            </div>
        </div>
        <!-- end col -->
    </div>
</asp:Content>
