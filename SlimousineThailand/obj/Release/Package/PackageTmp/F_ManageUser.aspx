﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Slimousine.Master" AutoEventWireup="true" CodeBehind="F_ManageUser.aspx.cs" Inherits="SlimousineThailand.F_ManageUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">MANAGE USER</h4>
        </div>
    </div>


    <!-- Bootstrap Modals -->
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <%--<h4 class="header-title m-t-0 m-b-30">Bootstrap Modals (default)</h4>--%>

                <p class="text-muted m-b-15 font-13">
                    Add UserSaklimousineThailand
                </p>
                <div id="dd" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title">Member</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">Username</label>
                                            <%--<input type="text" class="form-control" id="field-1" placeholder="John">--%>
                                            <asp:TextBox ID="txt_username_Emp" runat="server" class="form-control" placeholder="Username"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="field-2" class="control-label">Password</label>
                                            <asp:TextBox ID="txt_password_Emp" runat="server" class="form-control" placeholder="Password" TextMode="Password"></asp:TextBox>
                                            <%--<input type="text" class="form-control" id="field-2" placeholder="Password">--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">First Name</label>
                                            <%--<input type="text" class="form-control" id="field-1" placeholder="John">--%>
                                            <asp:TextBox ID="txt_fname_Emp" runat="server" class="form-control" placeholder="First Name"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="field-2" class="control-label">Last Name</label>
                                            <asp:TextBox ID="txt_lname_Emp" runat="server" class="form-control" placeholder="Last Name"></asp:TextBox>
                                            <%--<input type="text" class="form-control" id="field-2" placeholder="Password">--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="field-3" class="control-label">Address</label>
                                            <asp:TextBox ID="txt_Address_emp" runat="server" class="form-control" placeholder="Address"></asp:TextBox>
                                            <%--<input type="text" class="form-control" id="field-3" placeholder="สถานที่ติดต่อ-อาคาร 1 ชั้น 4 คณะเทคโนโลยีสารสนเทศ">--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="field-4" class="control-label">Phone</label>
                                            <asp:TextBox ID="txt_tell_Emp" runat="server" class="form-control" placeholder="Phone"></asp:TextBox>
                                            <%--<input type="text" class="form-control" id="field-4" placeholder="เบอร์มือถือ">--%>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="field-5" class="control-label">E-Mail</label>
                                            <asp:TextBox ID="txt_email_Emp" runat="server" class="form-control" placeholder="E-Mail  @nortbankok.com"></asp:TextBox>
                                            <%--<input type="text" class="form-control" id="field-5" placeholder="E-Mail  @nortbankok.com">--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group no-margin">
                                            <label for="field-7" class="control-label">Descriptions</label>
                                            <asp:TextBox ID="txt_description_Emp" runat="server" TextMode="MultiLine" class="form-control autogrow" placeholder="Descriptions"></asp:TextBox>
                                            <%--<textarea class="form-control autogrow" id="field-7" placeholder="Write something about yourself" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;"></textarea>--%>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <%--<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">ยกเลิก</button>--%>
                                <asp:Button ID="btn_Clear_emp" OnClick="btn_Clear_emp_Click" runat="server" Text="ยกเลิก" CssClass="btn btn-default" />
                                <asp:Button ID="btn_Save_emp" runat="server" OnClick="btn_Save_emp_Click" Text="บันทึก" CssClass="btn btn-info" />
                                <%--<asp:button type="button" class="btn btn-info waves-effect waves-light" ID="btn_Save_Emp" >Save changes</asp:button>--%>
                                <%--<asp:Button ID="btn_Save_Emp" runat="server" Text="Save changes" CssClass="btn btn-info waves-effect waves-light" />--%>
                                <%--<button type="button" class="btn btn-info waves-effect waves-light">Save changes</button>--%>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.modal -->

                <div class="button-list">
                    <%--<button type="button" class="btn btn-custom dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Settings <span class="m-l-5"><i class="fa fa-cog"></i></span></button>--%>
                    <asp:LinkButton ID="btn_add_user" class="btn btn-custom dropdown-toggle waves-effect waves-light" runat="server" OnClick="btn_add_user_Click">AddUser<span class="m-l-5"><i class="fa fa-cog"></i></span></asp:LinkButton>
                </div>
            </div>
        </div>
    </div>


    <!-- Custom Modals -->
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">


                <h4 class="header-title m-t-0 m-b-30">EMPLOYEE</h4>

             <%--   <p class="text-muted m-b-15 font-13">
                    Examples of custom modals.
                </p>--%>

                <div class="form-group">
                    <asp:GridView ID="grd_memdetial" runat="server" CssClass="table table-striped table-bordered dfdf" GridLines="None" EmptyDataText="ไม่มีรายการ"
                                AutoGenerateColumns="False" >
                                        <Columns>
                                    <asp:TemplateField >
                                        <HeaderStyle Width="80px"  />
                                        <ItemTemplate>
                                            <asp:HiddenField ID="HdnID" runat="server" Value='<%# Eval("MemberID") %>'></asp:HiddenField>
                                            <%--<asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" Width="20px"
                                                ImageUrl="~/img/Edit.png"  Text="Edit"  />&nbsp;
                                            <asp:ImageButton ID="imgDelete" runat="server" CssClass="col-md-2" CausesValidation="False"  Width="20px"
                                                OnClientClick="return confirm('ยืนยันการลบข้อมูล');" CommandName="Delete"
                                                ImageUrl="~/img/delete.png" Text="Delete"   />--%>
                                            <asp:LinkButton ID="btn_edit_mem" CausesValidation="False" OnClick="btn_edit_mem_Click"   CssClass="btn btn-icon waves-effect waves-light btn-warning m-b-5" runat="server"  ><span class="fa fa-wrench"></span></asp:LinkButton>
                                            <asp:LinkButton ID="btn_delete_mem" CausesValidation="False" OnClick="btn_delete_mem_Click"  OnClientClick="return confirm('ยืนยันการลบข้อมูล');" CssClass="btn btn-icon waves-effect waves-light btn-danger m-b-5" runat="server"  ><span class="fa fa-remove"></span></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle Width="80px" />
                                    </asp:TemplateField>
                                    <%--<asp:BoundField DataField="memid" HeaderText="ID" />--%>
                                    <asp:BoundField DataField="Firstname" HeaderText="Firstname" />
                                    <asp:BoundField DataField="Lastname" HeaderText="Lastname" />
                                    <asp:BoundField DataField="Username" HeaderText="Username" />
                                    <asp:BoundField DataField="Email" HeaderText="E-Mail" />
                                    <asp:BoundField DataField="Phone" HeaderText="PhoneNumer" />
                                </Columns>
                                <RowStyle CssClass="cursor-pointer" />


                                    </asp:GridView>
                </div>

            </div>
        </div>
        <!-- end col -->
    </div>
    <!-- End row -->

    



</asp:Content>
