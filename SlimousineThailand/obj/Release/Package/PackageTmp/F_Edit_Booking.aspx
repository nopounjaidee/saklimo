﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Slimousine.Master" AutoEventWireup="true" CodeBehind="F_Edit_Booking.aspx.cs" Inherits="SlimousineThailand.F_Edit_Booking" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Page-Title -->


    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-lg-6">
                        <div action="#" class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-5 control-label">BOOKING ID</label>
                                <div class="col-sm-7">
                                    <%--<a href="#" id="inline-username" data-type="text" data-pk="1" data-title="Enter username">superuser</a>--%>
                                    <asp:Label ID="lbl_idbook_edb" class="col-sm-4 control-label" runat="server" Text="NON"></asp:Label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Booking Date</label>
                                <div class="col-sm-7">
                                    <asp:Label ID="lbl_datebook_edb" class="col-sm-5 control-label" runat="server" Text="11/19/2018"></asp:Label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Trip</label>
                                <div class="col-sm-7">
                                    <asp:Label ID="lbl_trip_edb" class="col-sm-10 control-label" runat="server" Text="?? To ??"></asp:Label>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- end col -->

                    <div class="col-lg-6">
                        <div action="#" class="form-horizontal editor-horizontal">
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Booking Status</label>
                                <div class="col-sm-7">
                                    <asp:Label ID="lbl_booksatus_edb" class="col-sm-3 control-label" runat="server" Text="Wait" ForeColor="Blue"></asp:Label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Payment Status</label>
                                <div class="col-sm-7">
                                    <asp:Label ID="lbl_paybook_edb" class="col-sm-3 control-label" runat="server" Text="Wait" ForeColor="Blue"></asp:Label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Booking Update</label>
                                <div class="col-sm-7">
                                    <asp:Label ID="lbl_bookupdate_edb" class="col-sm-5 control-label" runat="server" Text="NON" ForeColor="Blue"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end col -->
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div action="#" class="form-horizontal">

                            
                        </div>
                    </div>

                </div>
                <!-- end row -->
            </div>
        </div>
        <!-- end col -->
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-6">
            <div class="card-box">


                <h4 class="header-title m-t-0 m-b-30">Service Detail</h4>

                <div class="control-group">
                    <fieldset>
                        <legend>Origin :</legend>
                        <div class="input-group has-success m-t-10" style="width: 100%">
                            <asp:DropDownList ID="ddl_glocals_edb" OnSelectedIndexChanged="ddl_glocals_edb_SelectedIndexChanged" runat="server" class="form-control" AutoPostBack="True">
                                <%--<asp:ListItem>Suvannapum </asp:ListItem>
                                <asp:ListItem>Donmuang</asp:ListItem>
                                <asp:ListItem>Bankkok Zon</asp:ListItem>
                                <asp:ListItem>Out Off Bankkok</asp:ListItem>--%>
                            </asp:DropDownList>
                        </div>
                        <div class="input-group has-success m-t-10 " style="width: 100%">
                            <asp:DropDownList ID="ddl_locals_edb" runat="server" class="form-control" OnSelectedIndexChanged="ddl_locals_edb_SelectedIndexChanged" AutoPostBack="True">
                                <%-- <asp:ListItem>Suvannapum</asp:ListItem>
                                <asp:ListItem>Donmuang</asp:ListItem>
                                <asp:ListItem>Bankkok Zon</asp:ListItem>
                                <asp:ListItem>Out Off Bankkok</asp:ListItem>--%>
                            </asp:DropDownList>
                        </div>
                    </fieldset>
                </div>
                <div class="control-group m-t-30">
                    <div class="control-group">
                        <fieldset>
                            <legend>Destination :</legend>
                            <div class="input-group has-error m-t-10" style="width: 100%">
                                <asp:DropDownList ID="ddl_glocale_edb" OnSelectedIndexChanged="ddl_glocale_edb_SelectedIndexChanged" runat="server" class="form-control" AutoPostBack="True">
                                    <%-- <asp:ListItem>Suvannapum</asp:ListItem>
                                    <asp:ListItem>Donmuang</asp:ListItem>
                                    <asp:ListItem>Bankkok Zon</asp:ListItem>
                                    <asp:ListItem>Out Off Bankkok</asp:ListItem>--%>
                                </asp:DropDownList>
                            </div>
                            <div class="input-group has-error m-t-10 " style="width: 100%">
                                <asp:DropDownList ID="ddl_locale_edb" runat="server" class="form-control" OnSelectedIndexChanged="ddl_locale_edb_SelectedIndexChanged" AutoPostBack="True">
                                    <%--<asp:ListItem>Suvannapum</asp:ListItem>
                                    <asp:ListItem>Donmuang</asp:ListItem>
                                    <asp:ListItem>Bankkok Zon</asp:ListItem>
                                    <asp:ListItem>Out Off Bankkok</asp:ListItem>--%>
                                </asp:DropDownList>
                            </div>
                        </fieldset>
                    </div>

                </div>
                <div class="control-group m-t-30">
                    <div class="control-group">
                        <div class="controls">
                            <label>CarType</label>

                            <div class="radio radio-success">
                                <asp:RadioButton ID="rdo_japan_edb" runat="server" Text="Japanese Luxury Sedan (Camry)" GroupName="Cartype" Checked="true" AutoPostBack="True" />
                            </div>

                            <div class="radio radio-success">
                                <asp:RadioButton ID="rdo_normal_edb" runat="server" Text="Normal Van" GroupName="Cartype" AutoPostBack="True" />
                            </div>

                            <div class="radio radio-success">
                                <asp:RadioButton ID="rdo_standard_edb" runat="server" Text="Standard Luxury Sedan" GroupName="Cartype" AutoPostBack="True" />
                            </div>

                            <div class="radio radio-success">
                                <asp:RadioButton ID="rdo_topsedan_edb" runat="server" Text="Top Luxury Sedan" GroupName="Cartype" AutoPostBack="True" />
                            </div>

                            <div class="radio radio-success">
                                <asp:RadioButton ID="rdo_topvan_edb" runat="server" Text="Top Luxury Van (Benz Vito)" GroupName="Cartype" AutoPostBack="True" />
                            </div>

                            <div class="radio radio-success">
                                <asp:RadioButton ID="rdo_vipvan_edb" runat="server" Text="Vip Van" GroupName="Cartype" AutoPostBack="True" />
                            </div>

                        </div>
                    </div>

                </div>

                <div class="control-group m-t-30">
                    <div class="control-group">
                        <div role="form" class="form-horizontal">
                            <div class="form-group has-success">
                                <%--<label class="col-md-3 control-label">Affiliate </label>--%>
                                <label class="col-sm-3 control-label" for="example-input-normal">Vehicle</label>

                                <div class="col-md-6">
                                    <asp:DropDownList ID="ddl_vehicle_edb" runat="server" CssClass="form-control">
                                        <%--<asp:ListItem Value="BMW"></asp:ListItem>
                                        <asp:ListItem Value="BEN"></asp:ListItem>--%>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="control-group m-t-30">
                    <div class="control-group">
                        <fieldset>
                            <legend></legend>
                            <div class="form-group">
                                <label class="control-label" for="example-input1-group2">Limou Price</label>
                                <div class="input-group m-t-10 col-lg-4">
                                    <asp:TextBox ID="txt_limoprice_edb" OnTextChanged="txt_limoprice_edb_TextChanged" runat="server" class="input-mini form-control "></asp:TextBox>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn waves-effect waves-light btn-primary">Baht</button>
                                    </span>
                                </div>
                            </div>
                            <!-- form-group -->
                            <div class="form-group">
                                <label class="control-label" for="example-input1-group2">By Day Service</label>
                                <div class="input-group  ">
                                </div>
                            </div>
                            <div class="row  ">
                                <div class="col-lg-4">
                                    <div class="input-group m-t-10 ">
                                        <asp:TextBox ID="txt_dayservicP_edb" runat="server" OnTextChanged="txt_dayservicP_edb_TextChanged" class="input-mini form-control "></asp:TextBox>
                                        <span class="input-group-btn">
                                            <button type="button" class="btn waves-effect waves-light btn-primary">Price</button>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="input-group m-t-10 ">
                                        <asp:TextBox ID="txt_dayservicD_edb" runat="server" class="input-mini form-control "></asp:TextBox>
                                        <span class="input-group-btn">
                                            <button type="button" class="btn waves-effect waves-light btn-primary">Day</button>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-lg-4 ">
                                    <div class="input-group m-t-10 ">
                                        <asp:TextBox ID="txt_dayservicB_edb" runat="server" class="input-mini form-control " ReadOnly=""></asp:TextBox>
                                        <span class="input-group-btn">
                                            <button type="button" class="btn waves-effect waves-light btn-primary">Baht</button>
                                        </span>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group m-t-10 ">
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="example-input1-group2">By Hour Service</label>
                                <div class="input-group  ">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="input-group m-t-10 ">
                                        <asp:TextBox ID="txt_hourservicP_edb" runat="server" class="input-mini form-control "></asp:TextBox>
                                        <span class="input-group-btn">
                                            <button type="button" class="btn waves-effect waves-light btn-primary">Price</button>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="input-group m-t-10 ">
                                        <asp:TextBox ID="txt_hourservicH_edb" runat="server" class="input-mini form-control "></asp:TextBox>
                                        <span class="input-group-btn">
                                            <button type="button" class="btn waves-effect waves-light btn-primary">Hour</button>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-lg-4 ">
                                    <div class="input-group m-t-10 ">
                                        <asp:TextBox ID="txt_hourservicB_edb" runat="server" class="input-mini form-control " ReadOnly=""></asp:TextBox>
                                        <span class="input-group-btn">
                                            <button type="button" class="btn waves-effect waves-light btn-primary">Baht</button>
                                        </span>
                                    </div>
                                </div>

                            </div>


                        </fieldset>
                        <div class="form-group m-t-10 ">
                            .
                        </div>

                        <div class="form-group m-t-10 ">
                            <label class="col-sm-3 control-label" for="example-input-normal">Service Remark</label>
                            <div class="col-sm-9">
                                <asp:TextBox ID="txt_servicremack_edb" runat="server" TextMode="MultiLine" class="input-mini form-control "></asp:TextBox>

                            </div>
                        </div>
                        <div class="form-group m-t-10 ">
                            .
                        </div>
                        <div role="form" class="form-horizontal">
                            <div class="form-group has-success">
                                <%--<label class="col-md-3 control-label">Affiliate </label>--%>
                                <label class="col-sm-3 control-label" for="example-input-normal">Fast Track</label>

                                <div class="col-md-6">
                                    <asp:DropDownList ID="ddl_fasttrack_edb" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="Please select one"></asp:ListItem>
                                        <asp:ListItem Value="Normal Fast Track Service(Arrivarl or Departure)"></asp:ListItem>
                                        <asp:ListItem Value="VIP Fast Track Service Arrivarl"></asp:ListItem>
                                        <asp:ListItem Value="VIP Fast Track Service Departure"></asp:ListItem>
                                        <asp:ListItem Value="Special Cases"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class=" col-sm-3 control-label" for="example-input1-group2">Fast Track Price</label>
                                <div class="input-group  ">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="input-group m-t-10 ">
                                        <asp:TextBox ID="txt_fasttrackP_edb" runat="server" class="input-mini form-control "></asp:TextBox>
                                        <span class="input-group-btn">
                                            <button type="button" class="btn waves-effect waves-light btn-primary">Price</button>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="input-group m-t-10 ">
                                        <asp:TextBox ID="txt_fasttrackPack_edb" runat="server" class="input-mini form-control "></asp:TextBox>
                                        <span class="input-group-btn">
                                            <button type="button" class="btn waves-effect waves-light btn-primary">Pax</button>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-lg-4 ">
                                    <div class="input-group m-t-10 ">
                                        <asp:TextBox ID="txt_fasttrackB_edb" runat="server" class="input-mini form-control " ReadOnly=""></asp:TextBox>
                                        <span class="input-group-btn">
                                            <button type="button" class="btn waves-effect waves-light btn-primary">Baht</button>
                                        </span>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group m-t-10 ">
                                .
                            </div>
                            <div class="form-group  ">
                                <label class="col-sm-3 control-label" for="example-input-normal">Fast Track Remark</label>
                                <div class="col-sm-9">
                                    <asp:TextBox ID="txt_fasttrackremack_edb" runat="server" TextMode="MultiLine" class="input-mini form-control "></asp:TextBox>

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="example-input-normal">Car No</label>
                                <div class="col-sm-6">
                                    <asp:TextBox ID="txt_carno_edb" runat="server" class="input-mini form-control "></asp:TextBox>

                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="example-input1-group2">Grand Total</label>
                            <div class="input-group m-t-10 col-lg-4">
                                <asp:TextBox ID="txt_totalB_edb" runat="server" class="input-mini form-control " ReadOnly=""></asp:TextBox>
                                <span class="input-group-btn">
                                    <button type="button" class="btn waves-effect waves-light btn-primary">Baht</button>
                                </span>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <!-- end col -->

        <div class="col-lg-6">
            <div class="card-box">


                <h4 class="header-title m-t-0 m-b-30">Airport Detail</h4>

                <div role="form" class="form-horizontal">
                    <div class="form-group has-success">
                        <%--<label class="col-md-3 control-label">Affiliate </label>--%>
                        <label class="col-sm-3 control-label" for="example-input-normal">Affiliate</label>

                        <div class="col-md-6">
                            <asp:DropDownList ID="ddl_affiliate_edb" runat="server" OnSelectedIndexChanged="ddl_affiliate_edb_SelectedIndexChanged" CssClass="form-control" AutoPostBack="True">
                                <%-- <asp:ListItem Value="1"></asp:ListItem>
                                <asp:ListItem Value="2"></asp:ListItem>--%>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="example-input-normal">Flight No</label>
                        <div class="col-sm-6">
                            <asp:TextBox ID="txt_flightno_edb" runat="server" class="input-mini form-control "></asp:TextBox>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="example-input-normal">Pickup Date</label>
                        <div class="col-sm-6">
                            <asp:TextBox ID="txt_pickup_date_edb" runat="server" class="input-mini form-control datepicker-autoclose"></asp:TextBox>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="example-input-normal">Pick up time</label>
                        <div class="col-sm-6">
                            <asp:TextBox ID="txt_pickup_time_edb" runat="server" class="input-mini form-control timepicker2"></asp:TextBox>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Pickup Location</label>
                        <div class="col-sm-7">
                            <asp:Label ID="lbl_pickup_local_edb" class="col-sm-3 control-label" runat="server" Text="?? To ??"></asp:Label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="example-input-normal">Pickup Detail</label>
                        <div class="col-sm-6">
                            <asp:TextBox ID="txt_pickup_detail_edb" runat="server" TextMode="MultiLine" class="input-mini form-control "></asp:TextBox>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">DropOff Location</label>
                        <div class="col-sm-7">
                            <asp:Label ID="lbl_dropoff_local_edb" class="col-sm-3 control-label" runat="server" Text="?? To ??"></asp:Label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="example-input-normal">DropOff Detail</label>
                        <div class="col-sm-6">
                            <asp:TextBox ID="txt_dropoff_detail_edb" runat="server" TextMode="MultiLine" class="input-mini form-control "></asp:TextBox>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="example-input-normal">Contact Name</label>
                        <div class="col-sm-6">
                            <asp:TextBox ID="txt_contectname_edb" runat="server" class="input-mini form-control "></asp:TextBox>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="example-input-normal">Contact Email</label>
                        <div class="col-sm-6">
                            <asp:TextBox ID="txt_contectemail_edb" runat="server" class="input-mini form-control "></asp:TextBox>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="example-input-normal">Contact Phone</label>
                        <div class="col-sm-6">
                            <asp:TextBox ID="txt_contectphone_edb" runat="server" class="input-mini form-control "></asp:TextBox>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="example-input-normal">Contact Detail</label>
                        <div class="col-sm-6">
                            <asp:TextBox ID="txt_contextdetail_edb" runat="server" TextMode="MultiLine" class="input-mini form-control "></asp:TextBox>

                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- end col -->

        <div class="col-lg-6">
            <div class="card-box">
                <div class="form-group">
                    <label class="control-label" for="example-input1-group2">Driver Diteal</label>

                    <div class="input-group m-t-10 col-lg-8">
                        <%--<input type="email" id="Email1" name="example-input2-group2" class="form-control" placeholder="Search">--%>
                        <asp:TextBox ID="txt_Shrech_EMP_MS" runat="server" class="form-control" placeholder="Search"></asp:TextBox>
                        <span class="input-group-btn">
                            <%--<button type="button" class="btn waves-effect waves-light btn-primary"><i class="fa fa-search"></i>  เลือก</button>--%>
                            <asp:LinkButton ID="bnt_search_driv_edb" runat="server" OnClick="bnt_search_driv_edb_Click" type="button" class="btn waves-effect waves-light btn-primary"><i class="fa fa-search"></i> Search</asp:LinkButton>
                            <asp:LinkButton ID="bnt_add_drive_ed" runat="server" OnClick="bnt_add_drive_ed_Click" type="button" class="btn waves-effect waves-light btn-success"><i class="fa fa-plus"></i> Add Up</asp:LinkButton>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <asp:GridView ID="grd_Driver_ed" runat="server" CssClass="table table-striped table-bordered" GridLines="None" EmptyDataText="ไม่มีรายการ"
                        AutoGenerateColumns="False">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="col-md-1" />
                                <ItemTemplate>
                                    <asp:HiddenField ID="HIDDriv" runat="server" Value='<%# Eval("MemberID") %>'></asp:HiddenField>
                                    <asp:LinkButton ID="btn_Select_Driver_ed" OnClick="btn_Select_Driver_ed_Click" CausesValidation="False" CssClass="btn btn-icon waves-effect waves-light btn-purple m-b-5" runat="server"><span class="zmdi zmdi-assignment-returned"></span></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle Width="80px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="MemberID" HeaderText="ID" />
                            <asp:BoundField DataField="Name" HeaderText="Fullname" />
                            <asp:BoundField DataField="Phone" HeaderText="Phone" />
                            <asp:BoundField DataField="Email" HeaderText="E-Mail" />
                        </Columns>
                        <RowStyle CssClass="cursor-pointer" />
                    </asp:GridView>
                </div>
                <div class="row">
                    <div class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="col-md-3 control-label">NAME</label>
                            <div class="col-md-10 col-lg-8">
                                <asp:TextBox ID="txt_Name_driv_edb" runat="server" class="form-control" ReadOnly=""></asp:TextBox>
                                <%--<input type="text" class="form-control" readonly="" value="รหัสวิชา">--%>
                            </div>
                        </div>
                    </div>

                    <div class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="col-md-2 control-label">E-Mail</label>
                            <div class="col-md-10 col-lg-6">
                                <asp:TextBox ID="txt_Email_driv_edb" runat="server" class="form-control" ReadOnly=""></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">PHONE</label>
                            <div class="col-md-10 col-lg-6">
                                <asp:TextBox ID="txt_phone_driv_edb" runat="server" class="form-control" ReadOnly=""></asp:TextBox>
                            </div>
                        </div>
                        <%--<div class="form-group">
                            <label class="col-md-2 control-label">PHONE</label>
                            <div class="col-md-10 col-lg-6">
                                <asp:TextBox ID="TextBox18" runat="server" class="form-control" ReadOnly=""></asp:TextBox>
                            </div>
                        </div>--%>
                    </div>
                    <!-- end col -->


                </div>

            </div>
        </div>
        <!-- end col -->

        <div class="col-lg-6">
            <div class="card-box">
                <div class="form-group">
                    <label class="control-label" for="example-input1-group2">Passenger Diteal</label>
                    <div class="input-group m-t-10 col-lg-8">
                        <span class="input-group-btn">
                            <asp:LinkButton ID="btn_add_passen_edb" OnClick="btn_add_passen_edb_Click" runat="server" type="button" class="btn waves-effect waves-light btn-success"><i class="fa fa-plus"></i> Add Up</asp:LinkButton>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <asp:GridView ID="grd_passendetial_ed" runat="server" CssClass="table table-striped table-bordered " GridLines="None" EmptyDataText="ไม่มีรายการ"
                        AutoGenerateColumns="False">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderStyle Width="80px" />
                                <ItemTemplate>
                                    <%--<asp:HiddenField ID="HAddpassenID" runat="server" Value='<%# Eval("ID") %>'></asp:HiddenField>--%>
                                    <%--<asp:HiddenField ID="Hid_opid" runat="server" Value='<%# Eval("subopenID") %>'></asp:HiddenField>--%>
                                    <%--<asp:LinkButton ID="btn_edit_passen_cb" CausesValidation="False"     CssClass="btn btn-icon waves-effect waves-light btn-warning m-b-5" runat="server"  ><span class="fa fa-wrench"></span></asp:LinkButton>--%>
                                    <asp:LinkButton ID="btn_delete_passen_ed" OnClick="btn_delete_passen_ed_Click" CausesValidation="False" OnClientClick="return confirm('ยืนยันการลบข้อมูล');" CssClass="btn btn-icon waves-effect waves-light btn-danger m-b-5" runat="server"><span class="fa fa-remove"></span></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle Width="80px" />
                            </asp:TemplateField>
                            <%--<asp:BoundField DataField="memid" HeaderText="ID" />--%>
                            <asp:BoundField DataField="Fname" HeaderText="FirstName" />
                            <asp:BoundField DataField="Mname" HeaderText="MiddleName" />
                            <asp:BoundField DataField="Lname" HeaderText="LastName" />
                            <asp:BoundField DataField="Phon1" HeaderText="PhoneNumber" />
                        </Columns>
                        <RowStyle CssClass="cursor-pointer" />
                    </asp:GridView>
                </div>

            </div>
        </div>
        <!-- end col -->

        <div class="col-lg-6">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30">Update Status</h4>

                <div role="form" class="form-horizontal">
                    <div class="form-group has-success">
                        <%--<label class="col-md-3 control-label">Affiliate </label>--%>
                        <label class="col-sm-3 control-label" for="example-input-normal">Payment Status</label>

                        <div class="col-md-6">
                            <asp:DropDownList ID="ddl_satus_pay_edb" runat="server" CssClass="form-control">
                                <asp:ListItem Value="Wait"></asp:ListItem>
                                <asp:ListItem Value="Paid"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group has-success">
                        <%--<label class="col-md-3 control-label">Affiliate </label>--%>
                        <label class="col-sm-3 control-label" for="example-input-normal">Booking Status</label>

                        <div class="col-md-6">
                            <asp:DropDownList ID="ddl_status_book_edb" runat="server" CssClass="form-control">
                                <asp:ListItem Value="Wait"></asp:ListItem>
                                <asp:ListItem Value="Comfirm"></asp:ListItem>
                                <asp:ListItem Value="Complete"></asp:ListItem>
                                <asp:ListItem Value="Reject"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class=" control-label" for="example-input1-group2">Balance Price</label>
                    </div>
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="input-group m-t-10 ">
                                <asp:TextBox ID="txt_balanceP_edb" runat="server" class="input-mini form-control " ReadOnly=""></asp:TextBox>
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-inverse waves-effect w-md waves-light m-b-5">Baht</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class=" control-label" for="example-input1-group2">Rental Car</label>
                    </div>
                    <div class="row ">
                        <div class="col-lg-5">
                            <div class="input-group m-t-10 ">
                                <asp:TextBox ID="txt_rentalP_edb" runat="server" class="input-mini form-control "></asp:TextBox>
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-inverse waves-effect w-md waves-light m-b-5">Baht</button>
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="input-group m-t-10 ">
                                <span class="input-group-btn">
                                    <%--<button type="button" class="btn waves-effect waves-light btn-primary">= </button>--%>
                                    <asp:LinkButton ID="btn_touwgub_edb" OnClick="btn_touwgub_edb_Click" runat="server" class="btn waves-effect waves-light btn-primary"> = </asp:LinkButton>
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-5 ">
                            <div class="input-group m-t-10 ">
                                <asp:TextBox ID="txt_ENDPrice_edb" runat="server" class="input-mini form-control " ReadOnly=""></asp:TextBox>
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-inverse waves-effect w-md waves-light m-b-5">Baht</button>
                                </span>
                            </div>
                        </div>

                    </div>
                    <div class="form-group">
                        .
                    </div>
                    <div class="row">
                        <div class="col-lg-4"></div>
                        <div class="col-md-6">
                            <%--<button type="button" class="btn btn-primary waves-effect waves-light" id="showtoast">Submit</button>
                        <button type="button" class="btn btn-danger waves-effect waves-light" id="cleartoasts">Clear</button>--%>
                            <asp:LinkButton ID="btn_Confirm_Book_edb" runat="server" OnClick="btn_Confirm_Book_edb_Click" class="btn btn-primary waves-effect waves-light">Confirm</asp:LinkButton>
                            <asp:LinkButton ID="btn_Delete_Book_edb" runat="server" Visible="false" OnClick="btn_Delete_Book_edb_Click" OnClientClick="return confirm('Are you sure?');" class="btn btn-danger waves-effect waves-light">Delete</asp:LinkButton>
                            <asp:LinkButton ID="btn_SendMail_book_edb" runat="server" Visible="false" class="btn btn-success waves-effect waves-light">Send Mail</asp:LinkButton>
                        </div>
                    </div>

                </div>

            </div>
        </div>

        <div id="oo" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Diver</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">First Name</label>
                                    <%--<input type="text" class="form-control" id="field-1" placeholder="John">--%>
                                    <asp:TextBox ID="txt_fname_div_edb" runat="server" class="form-control" placeholder="First Name"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="field-2" class="control-label">Last Name</label>
                                    <asp:TextBox ID="txt_lname_div_edb" runat="server" class="form-control" placeholder="Last Name"></asp:TextBox>
                                    <%--<input type="text" class="form-control" id="field-2" placeholder="Password">--%>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Address</label>
                                    <asp:TextBox ID="txt_loca_div_edb" runat="server" class="form-control" placeholder="Address"></asp:TextBox>
                                    <%--<input type="text" class="form-control" id="field-3" placeholder="สถานที่ติดต่อ-อาคาร 1 ชั้น 4 คณะเทคโนโลยีสารสนเทศ">--%>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="field-4" class="control-label">Phone</label>
                                    <asp:TextBox ID="txt_tell_div_edb" runat="server" class="form-control" placeholder="Phone"></asp:TextBox>
                                    <%--<input type="text" class="form-control" id="field-4" placeholder="เบอร์มือถือ">--%>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="field-5" class="control-label">E-Mail</label>
                                    <asp:TextBox ID="txt_email_div_edb" runat="server" class="form-control" placeholder="E-Mail  @nortbankok.com"></asp:TextBox>
                                    <%--<input type="text" class="form-control" id="field-5" placeholder="E-Mail  @nortbankok.com">--%>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group no-margin">
                                    <label for="field-7" class="control-label">Descriptions</label>
                                    <asp:TextBox ID="txt_description_div_edb" runat="server" TextMode="MultiLine" class="form-control autogrow" placeholder="Descriptions"></asp:TextBox>
                                    <%--<textarea class="form-control autogrow" id="field-7" placeholder="Write something about yourself" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;"></textarea>--%>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <%--<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">ยกเลิก</button>--%>
                        <asp:Button ID="btn_Clear_emp" OnClick="btn_Clear_emp_Click" runat="server" Text="ยกเลิก" CssClass="btn btn-default" />
                        <asp:Button ID="btn_Save_emp" OnClick="btn_Save_emp_Click" runat="server" Text="บันทึก" CssClass="btn btn-info" />
                        <%--<asp:button type="button" class="btn btn-info waves-effect waves-light" ID="btn_Save_Emp" >Save changes</asp:button>--%>
                        <%--<asp:Button ID="btn_Save_Emp" runat="server" Text="Save changes" CssClass="btn btn-info waves-effect waves-light" />--%>
                        <%--<button type="button" class="btn btn-info waves-effect waves-light">Save changes</button>--%>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal -->
        <div id="ll" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Passenger</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="control-group">
                                <div class="controls col-md-4">
                                    <label for="showEasing">FirstName</label>
                                    <%--<input id="showEasing" type="text" placeholder="swing, linear" class="input-mini form-control" value="swing" />--%>
                                    <asp:TextBox ID="txt_fname_pas_edb" runat="server" class="input-mini form-control"></asp:TextBox>
                                </div>
                                <div class="controls col-md-3">
                                    <label for="showEasing">Middle</label>
                                    <asp:TextBox ID="txt_mname_pas_edb" runat="server" class="input-mini form-control"></asp:TextBox>
                                </div>
                                <div class="controls col-md-5">
                                    <label for="showEasing">LastName</label>
                                    <asp:TextBox ID="txt_lname_pas_edb" runat="server" class="input-mini form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="control-group">
                                <div class="controls">
                                    .

                                </div>
                                <div class="controls col-md-5">
                                    <label for="showEasing">Phone1</label>
                                    <%--<input id="showEasing" type="text" placeholder="swing, linear" class="input-mini form-control" value="swing" />--%>
                                    <asp:TextBox ID="txt_phone_pas_edb" runat="server" class="input-mini form-control"></asp:TextBox>
                                </div>
                                <div class="controls col-md-5">
                                    <label for="showEasing">Phone2</label>
                                    <asp:TextBox ID="txt_phone2_pas_edb" runat="server" class="input-mini form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="control-group">
                                <div class="controls">
                                    .

                                </div>
                                <div class="controls col-md-8">
                                    <label for="showEasing">E-mail</label>
                                    <%--<input id="showEasing" type="text" placeholder="swing, linear" class="input-mini form-control" value="swing" />--%>
                                    <asp:TextBox ID="txt_email_pas_edb" runat="server" class="input-mini form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <%--<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">ยกเลิก</button>--%>
                        <asp:Button ID="btn_cancle_edb" OnClick="btn_cancle_edb_Click" runat="server" Text="ยกเลิก" CssClass="btn btn-default" />
                        <asp:Button ID="btn_submit_addpassen_edb" OnClick="btn_submit_addpassen_edb_Click" runat="server" Text="บันทึก" CssClass="btn btn-info" />
                        <%--<asp:button type="button" class="btn btn-info waves-effect waves-light" ID="btn_Save_Emp" >Save changes</asp:button>--%>
                        <%--<asp:Button ID="btn_Save_Emp" runat="server" Text="Save changes" CssClass="btn btn-info waves-effect waves-light" />--%>
                        <%--<button type="button" class="btn btn-info waves-effect waves-light">Save changes</button>--%>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal -->

    </div>
    <!-- End row -->
</asp:Content>
