﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Slimousine.Master" AutoEventWireup="true" CodeBehind="F_Searchbook.aspx.cs" Inherits="SlimousineThailand.F_Searchbook" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">

            <h4 class="page-title">BOOKING SEARCH</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">


                <h4 class="header-title m-t-0 m-b-30">Date Picker</h4>

                <div class="row">
                    <div class="col-lg-8">

                        <div class="p-20">
                            <div action="#" class="form-horizontal">
                                <div class="form-group">
                                    <label class="control-label col-sm-4">DATE RANGE</label>
                                    <div class="col-sm-8">
                                        <div class="input-daterange input-group" id="date-range">
                                            <%--<input type="text" class="form-control" name="start" />--%>
                                            <asp:TextBox ID="txt_sdate_SB" runat="server" class="form-control"></asp:TextBox>
                                            <span class="input-group-addon bg-primary b-0 text-white">to</span>
                                            <asp:TextBox ID="txt_edate_SB" runat="server" class="form-control"></asp:TextBox>
                                            <%--<input type="text" class="form-control" name="end" />--%>
                                        </div>
                                    </div>
                                </div>
                              

                                <div class="form-group">
                                    <div class="col-sm-7 ">
                                        .
                                    </div>
                                    <div class="col-sm-5 ">
                                        <asp:LinkButton ID="btn_search_SB" OnClick="btn_search_SB_Click" runat="server" type="button" class="btn waves-effect waves-light btn-primary"><i class="fa fa-search"></i> Search</asp:LinkButton>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                    <!-- end col -->
                    <div class="col-lg-4">

                    </div>

                </div>
                <!-- end row-->
            </div>
        </div>
        <!-- end col -->
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">

                <%--<h4 class="header-title m-t-0 m-b-30">มคอ</h4>--%>
                <%--<asp:Label ID="lbl_HQTF" runat="server" Text="มคอ" class="header-title m-t-0 m-b-30"></asp:Label>--%>



                <asp:GridView ID="grd_searchbook" OnRowDataBound="grd_searchbook_RowDataBound" runat="server" GridLines="None" EmptyDataText="ไม่มีรายการ"
                    AutoGenerateColumns="False"  CssClass=" table table-striped table-bordered dfdf">

                    <Columns>

                        <asp:BoundField DataField="BookingID" HeaderText="BOOKING ID" />
                        <asp:BoundField DataField="Statlocation" HeaderText="Pickup" />
                        <asp:BoundField DataField="Endlocation" HeaderText="DropOff" />
                        <asp:BoundField DataField="FlightNo" HeaderText="Flight NO" />
                        <asp:BoundField DataField="Pickupdate" HeaderText="Pickup Date" DataFormatString="{0:dd/MM/yyyy}" />
                        <asp:BoundField DataField="Pickuptime" HeaderText="Pickup Time" />

                        <asp:TemplateField HeaderText="Booking Status">
                            <HeaderStyle/>
                            <ItemTemplate>
                                <%--<asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("qtfID") %>'></asp:HiddenField>--%>
                                <asp:HiddenField ID="Hid_bksid" runat="server" Value='<%# Eval("BookingID") %>'></asp:HiddenField>
                                <asp:HiddenField ID="Hid_bkdsstatus" runat="server" Value='<%# Eval("Bookingstatus") %>'></asp:HiddenField>
                                <%--<asp:HiddenField ID="hid_status" runat="server" Value='<%# Eval("status") %>'></asp:HiddenField>--%>
                                <asp:LinkButton ID="lbtn_a" OnClick="lbtn_a_Click" CausesValidation="False" CssClass="btn btn-icon waves-effect waves-light btn-warning m-b-5" runat="server"><span class="fa fa-wrench"> Wait.</span></asp:LinkButton>
                                <asp:LinkButton ID="lbtn_d" OnClick="lbtn_d_Click" CausesValidation="False" CssClass="btn btn-icon waves-effect btn-danger m-b-5" runat="server"><i class="fa fa-wrench" style="width: auto;margin-right: 10px;"></i> Reject</span></asp:LinkButton>
                                <asp:LinkButton ID="lbtn_b" OnClick="lbtn_b_Click" CausesValidation="False" OnClientClick="return confirm('Comfirm & SendMail ?');" CssClass="btn btn-icon waves-effect waves-light btn-primary m-b-5" runat="server"><i class="fa fa-spin fa-spinner" style="width: auto;margin-right: 10px;"></i> Comfirm</span></asp:LinkButton>
                                <asp:LinkButton ID="lbtn_c" OnClick="lbtn_c_Click" CausesValidation="False" CssClass="btn btn-icon waves-effect waves-light btn-success m-b-5" runat="server"><span class="zmdi zmdi-check-circle"> Complete</span></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Contactname" HeaderText="Contect Name" />
                        <asp:BoundField DataField="Contactphone" HeaderText="Contect Phone" />

                        <%--<asp:BoundField DataField="status" HeaderText="status" />--%>
                    </Columns>
                    <RowStyle CssClass="cursor-pointer" />

                </asp:GridView>
            </div>
        </div>
        <!-- end col -->
    </div>

</asp:Content>
