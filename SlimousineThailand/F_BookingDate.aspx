﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Slimousine.Master" AutoEventWireup="true" CodeBehind="F_BookingDate.aspx.cs" Inherits="SlimousineThailand.F_BookingDate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">

            <h4 class="page-title">BOOKING LIST</h4>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">

                <%--<h4 class="header-title m-t-0 m-b-30">มคอ</h4>--%>
                <%--<asp:Label ID="lbl_HQTF" runat="server" Text="มคอ" class="header-title m-t-0 m-b-30"></asp:Label>--%>



                <asp:GridView ID="grd_searchbookdate" OnRowDataBound="grd_searchbookdate_RowDataBound" runat="server" GridLines="None" EmptyDataText="ไม่มีรายการ"
                    AutoGenerateColumns="False" CssClass=" table table-striped table-bordered dfdf">

                    <Columns>

                        <asp:BoundField DataField="BookingID" HeaderText="BOOKING ID" />
                        <asp:BoundField DataField="Pickuplocation" HeaderText="PickupLocation" />
                        <asp:BoundField DataField="DropOfflocation" HeaderText="DropOffLocation" />
                        <asp:BoundField DataField="passname" HeaderText="Passenger" />
                        <asp:BoundField DataField="FlightNo" HeaderText="Flight NO" />
                        <asp:BoundField DataField="Pickupdate" HeaderText="Pickup Date" DataFormatString="{0:dd/MM/yyyy}" />
                        <asp:BoundField DataField="Pickuptime" HeaderText="Pickup Time" />

                        <asp:TemplateField HeaderText="Booking Status">
                            <HeaderStyle />
                            <ItemTemplate>
                                <asp:HiddenField ID="Hid_bkid" runat="server" Value='<%# Eval("BookingID") %>'></asp:HiddenField>
                                <asp:HiddenField ID="Hid_bkdsid" runat="server" Value='<%# Eval("Bookingstatus") %>'></asp:HiddenField>
                                <%--<asp:HiddenField ID="hid_status" runat="server" Value='<%# Eval("status") %>'></asp:HiddenField>--%>
                                <asp:LinkButton ID="lbtn_edit_bkb"  OnClick="lbtn_edit_bkb_Click" CausesValidation="False" CssClass="btn btn-rounded w-md waves-effect waves-light btn-inverse m-b-5" runat="server"><i class="fa fa-spin fa-spinner" style="width: auto;margin-right: 10px;"></i> Pending...</span></asp:LinkButton>
                                <asp:LinkButton ID="lbtn_reject_bkb" OnClick="lbtn_reject_bkb_Click" CausesValidation="False" CssClass="btn btn-rounded w-md waves-effect waves-light btn-danger m-b-5" runat="server"><i class="fa fa-times-circle" style="width: auto;margin-right: 10px;"></i> Cancel</span></asp:LinkButton>
                                <asp:LinkButton ID="lbtn_confirm_bkb" OnClick="lbtn_confirm_bkb_Click" CausesValidation="False" CssClass="btn btn-rounded w-md waves-effect waves-light btn-primary m-b-5" runat="server"><i class="fa fa-thumbs-o-up" style="width: auto;margin-right: 10px;"></i> Confirm </span></asp:LinkButton>
                                <asp:LinkButton ID="lbtn_complete_bkb" OnClick="lbtn_complete_bkb_Click" CausesValidation="False" CssClass="btn btn-rounded w-md waves-effect waves-light btn-success m-b-5" runat="server"><i class="fa fa-check" style="width: auto;margin-right: 10px;"></i> Complete</span></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Serviceremark" HeaderText="ServiceRemark" />
                        <%--<asp:BoundField DataField="Paymentstatus" HeaderText="PaymentStatus" />--%>
                        <asp:TemplateField HeaderText="PaymentStatus">
                            <HeaderStyle />
                            <ItemTemplate>
                                <asp:HiddenField ID="Hid_Peyments" runat="server" Value='<%# Eval("Paymentstatus") %>'></asp:HiddenField>
                                <%--<asp:HiddenField ID="hid_status" runat="server" Value='<%# Eval("status") %>'></asp:HiddenField>--%>
                                <%--<span class="label label-danger">Cash</span>
                                <span class="label label-success">Credit Card</span>
                                <span class="label label-primary">Billing</span>--%>
                                <%--<span class="label label-warning">Money Transfer</span>--%>
                                <asp:Label ID="lbl_cach" runat="server" class="label label-danger" Text="Cash"></asp:Label>
                                <asp:Label ID="lbl_cradit_card" runat="server" class="label label-success" Text="Credit Card"></asp:Label>
                                <asp:Label ID="lbl_biling" runat="server" class="label label-primary" Text="Billing"></asp:Label>
                                <asp:Label ID="lbl_monytranfer" runat="server" class="label label-warning" Text="Money Transfer"></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <%--<asp:BoundField DataField="status" HeaderText="status" />--%>
                    </Columns>
                    <RowStyle CssClass="cursor-pointer" />

                </asp:GridView>
            </div>
        </div>
        <!-- end col -->
    </div>
</asp:Content>
