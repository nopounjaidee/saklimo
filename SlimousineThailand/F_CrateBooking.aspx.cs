﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SlimousineThailand.CS;

namespace SlimousineThailand
{
    public partial class F_CrateBooking : System.Web.UI.Page
    {
        static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
        private DataSet ds;
        static DataTable passendetail;
        SqlCommand sqlcomm = new SqlCommand();
        static string rdocartype = "";
        static string rdoservice = "";
        static string slo = "";
        static string elo = "";
        static string Glocals = "";
        static string Glocale = "";
        static string prefigg = "";
        static string fname = "";
        static string mname = "";
        static string lname = "";
        CS.Login Memid = new CS.Login();

        protected void Page_Load(object sender, EventArgs e)
        {


            if (!Page.IsPostBack)
            {
                
                SetDDLGlocals();
                SetDDLGlocale();
                grd_passendetial.DataSource = setdata();
                grd_passendetial.DataBind();
                txt_pickup_time_cb.Text = "";
                txt_pickup_time_cb.Text = "";

            }

        }

        private void InsetDatatable(string Prefigg, string Fname, string Mname, string Lname, string Phon1, string Phone2, string Email)
        {
            passendetail.Rows.Add(Prefigg , Fname, Mname, Lname, Phon1, Phone2, Email);
            grd_passendetial.DataSource = passendetail;
            grd_passendetial.DataBind();
        }
        public DataTable setdata()
        {

            passendetail = new DataTable();

            passendetail.Columns.Add("Prefigg", typeof(string));
            passendetail.Columns.Add("Fname", typeof(string));
            passendetail.Columns.Add("Mname", typeof(string));
            passendetail.Columns.Add("Lname", typeof(string));
            passendetail.Columns.Add("Phon1", typeof(string));
            passendetail.Columns.Add("Phon2", typeof(string));
            passendetail.Columns.Add("Email", typeof(string));


            return passendetail;


        }
        private void GetRDOCartype()
        {
            if (rdo_japan_cb.Checked)
            {
                rdocartype = "V1";
            }
            else if (rdo_normal_cb.Checked)
            {
                rdocartype = "V2";
            }
            else if (rdo_standard_cb.Checked)
            {
                rdocartype = "V3";
            }
            else if (rdo_topsededan_cb.Checked)
            {
                rdocartype = "V4";
            }
            else if (rdo_topvan_cb.Checked)
            {
                rdocartype = "V5";
            }
            else
            {
                rdocartype = "V6";
            }

        }
        private void SetDDLlocas()
        {



        }
        private void SetDDLGlocals()
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        // This will be your input parameter and its value
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;

                        //sqlcomm.Parameters.AddWithValue("@id", "1");

                        sqlcomm.CommandText = "spr_Setddl_Createbook";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ddl_glocals_cb.DataSource = ds.Tables[0];
                            ddl_glocals_cb.DataTextField = "Name";
                            ddl_glocals_cb.DataValueField = "GlocationID";
                            ddl_glocals_cb.DataBind();
                        }
                        else
                        {
                            ddl_glocals_cb.DataSource = null;
                            ddl_glocals_cb.DataBind();
                        }

                    }
                }
                //catch (SQLException ex)
                //{
                //    Console.WriteLine("SQL Error: " + ex.Message);
                //}
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
                Glocals = ddl_glocals_cb.SelectedItem.ToString();
                SetDDLlocals(ddl_glocals_cb.SelectedValue);
            }
        }
        private void SetDDLGlocale()
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        // This will be your input parameter and its value
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;

                        //sqlcomm.Parameters.AddWithValue("@id", "1");

                        sqlcomm.CommandText = "spr_Setddl_Createbook";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ddl_Glocae_cb.DataSource = ds.Tables[0];
                            ddl_Glocae_cb.DataTextField = "Name";
                            ddl_Glocae_cb.DataValueField = "GlocationID";
                            ddl_Glocae_cb.DataBind();
                        }
                        else
                        {
                            ddl_Glocae_cb.DataSource = null;
                            ddl_Glocae_cb.DataBind();
                        }

                    }
                }
                //catch (SQLException ex)
                //{
                //    Console.WriteLine("SQL Error: " + ex.Message);
                //}
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
                Glocale = ddl_Glocae_cb.SelectedItem.ToString();
                SetDDLlocale(ddl_Glocae_cb.SelectedValue);
            }
        }
        private void SetDDLlocals(string IDlocals)
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        // This will be your input parameter and its value
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;

                        //sqlcomm.Parameters.AddWithValue("@id", "1");
                        sqlcomm.Parameters.AddWithValue("@IDlocals", IDlocals);

                        sqlcomm.CommandText = "spr_SetDDL_locasCB";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ddl_locals_cb.DataSource = ds.Tables[0];
                            ddl_locals_cb.DataTextField = "Name";
                            ddl_locals_cb.DataValueField = "LocationID";
                            ddl_locals_cb.DataBind();
                        }
                        else
                        {
                            ddl_locals_cb.DataSource = null;
                            ddl_locals_cb.DataBind();
                        }

                    }
                }

                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
                slo = ddl_locals_cb.SelectedItem.ToString();
            }
        }
        private void SetDDLlocale(string IDlocals)
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        // This will be your input parameter and its value
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;

                        //sqlcomm.Parameters.AddWithValue("@id", "1");
                        sqlcomm.Parameters.AddWithValue("@IDlocals", IDlocals);

                        sqlcomm.CommandText = "spr_SetDDL_locasCB";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ddl_locae_cb.DataSource = ds.Tables[0];
                            ddl_locae_cb.DataTextField = "Name";
                            ddl_locae_cb.DataValueField = "LocationID";
                            ddl_locae_cb.DataBind();
                        }
                        else
                        {
                            ddl_locae_cb.DataSource = null;
                            ddl_locae_cb.DataBind();
                        }

                    }
                }

                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
                elo = ddl_locae_cb.SelectedItem.ToString();

            }
        }
        private void InsertBookink()
        {
            //DataTable dtinsert = null;
            //string ff = "";
            for (int i = 0; i < passendetail.Rows.Count; i++)
            {
                prefigg = passendetail.Rows[i]["Prefigg"].ToString();
                fname = passendetail.Rows[i]["Fname"].ToString();
                mname = passendetail.Rows[i]["Mname"].ToString();
                lname = passendetail.Rows[i]["Lname"].ToString();
                i = 20;

            }
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        sqlcomm.Parameters.AddWithValue("@VehicleID", rdocartype);
                        sqlcomm.Parameters.AddWithValue("@MemberID", Memid.getMemID());
                        sqlcomm.Parameters.AddWithValue("@Pickupdate", txt_pickup_date_cb.Text.ToString());
                        sqlcomm.Parameters.AddWithValue("@Pickuptime", txt_pickup_time_cb.Text);
                        sqlcomm.Parameters.AddWithValue("@StatlocationID", ddl_locals_cb.SelectedValue);
                        sqlcomm.Parameters.AddWithValue("@Statlocation", slo);
                        sqlcomm.Parameters.AddWithValue("@EndlocationID", ddl_locae_cb.SelectedValue);
                        sqlcomm.Parameters.AddWithValue("@Endlocation", elo);
                        sqlcomm.Parameters.AddWithValue("@Prefig", prefigg);
                        sqlcomm.Parameters.AddWithValue("@Firstname", fname);
                        sqlcomm.Parameters.AddWithValue("@Meddlename", mname);
                        sqlcomm.Parameters.AddWithValue("@LastName", lname);


                        sqlcomm.CommandText = "spr_Inset_Booking";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            string Idbook = "";
                            //modeUpdate = true;
                            Idbook = ds.Tables[0].Rows[0]["BookingID"].ToString();

                            for (int i = 0; i < passendetail.Rows.Count; i++)
                            {
                                string Prefigg = "";
                                string Fnames = "";
                                string Mname = "";
                                string Lname = "";
                                string Phon1 = "";
                                string Phon2 = "";
                                string Email = "";
                                Prefigg = passendetail.Rows[i]["Prefigg"].ToString();
                                Fnames = passendetail.Rows[i]["Fname"].ToString();
                                Mname = passendetail.Rows[i]["Mname"].ToString();
                                Lname = passendetail.Rows[i]["Lname"].ToString();
                                Phon1 = passendetail.Rows[i]["Phon1"].ToString();
                                Phon2 = passendetail.Rows[i]["Phon2"].ToString();
                                Email = passendetail.Rows[i]["Email"].ToString();
                                InsertPassen(Idbook,Prefigg, Fnames, Mname, Lname, Email, Phon1, Phon2);
                                //ProductId = GetDataSet_insertproduct(Sex, Producktype, Produckname, Produckdetails, Producksize, Produckcolor, Produckqty, Produckprice);
                                //InsertPassen();

                            }

                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "showModal();", true);
                        }
                        else
                        {
                            //modeUpdate = false;
                            //this.Clear();
                        }
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Record Inserted Successfully')", true);


                    }


                }
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
                Clear();
                Response.Redirect("/F_BookingDate.aspx");

            }
        }
        private void InsertPassen(string IDbook,string Prefigg ,string fname, string mname, string lname, string email, string phone1, string phone2)
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        sqlcomm.Parameters.AddWithValue("@BookingID", IDbook);
                        sqlcomm.Parameters.AddWithValue("@Prefig", Prefigg);
                        sqlcomm.Parameters.AddWithValue("@Firstname", fname);
                        sqlcomm.Parameters.AddWithValue("@Meddlename", mname);
                        sqlcomm.Parameters.AddWithValue("@LastName", lname);
                        sqlcomm.Parameters.AddWithValue("@Email",email );
                        sqlcomm.Parameters.AddWithValue("@Phone1",phone1 );
                        sqlcomm.Parameters.AddWithValue("@Phone2", phone2);


                        sqlcomm.CommandText = "spr_Insert_Passen";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);

                        //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Record Inserted Successfully')", true);


                    }


                }
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
                Clear();
            }
        }
        private void Clear()
        {
            txt_Fname_cb.Text = string.Empty;
            txt_Middlename_cb.Text = string.Empty;
            txt_Lastname_cb.Text = string.Empty;
            txt_phone1_cb.Text = string.Empty;
            txt_phone2_cb.Text = string.Empty;
            txt_email_cb.Text = string.Empty;
        }

        protected void btn_Addpassen_cb_Click(object sender, EventArgs e)
        {
           
            string fname = txt_Fname_cb.Text;
            string mname = txt_Middlename_cb.Text;
            string lname = txt_Lastname_cb.Text;
            string phone1 = txt_phone1_cb.Text;
            string phon2 = txt_phone2_cb.Text;
            string email = txt_email_cb.Text;
            string Prefigg = "";
            if (rdo_Mr_cb.Checked == true)
            {
                Prefigg = "Mr";
            }
            else
            {
                Prefigg = "Ms";
            }
            if (fname == "" && mname == "" && lname == "" && phone1 == "" && phon2 == "" && email == "")
            {

            }
            else
            {
                InsetDatatable(Prefigg, fname, mname, lname, phone1, phon2, email);
            }
            Clear();

        }

        protected void btn_delete_passen_cb_Click(object sender, EventArgs e)
        {
            if (passendetail.Rows.Count >= 1)
            {
                passendetail.Rows[((GridViewRow)((LinkButton)sender).NamingContainer).RowIndex].Delete();

            }
            grd_passendetial.DataSource = passendetail;
            grd_passendetial.DataBind();
            Clear();
        }

        protected void btn_Booking_cb_Click(object sender, EventArgs e)
        {
            slo = ddl_locals_cb.SelectedItem.ToString();
            elo = ddl_locae_cb.SelectedItem.ToString();
            GetRDOCartype();
            InsertBookink();
        }

        protected void btn_clear_cb_Click(object sender, EventArgs e)
        {
            Response.Redirect("/F_CrateBooking.aspx");
        }

        protected void ddl_glocals_cb_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetDDLlocals(ddl_glocals_cb.SelectedValue);
        }

        protected void ddl_Glocae_cb_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetDDLlocale(ddl_Glocae_cb.SelectedValue);
        }
    }
}