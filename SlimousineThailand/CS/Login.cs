﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SlimousineThailand.CS
{
    public class Login
    {
        static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
        private DataSet ds;
        SqlCommand sqlcomm = new SqlCommand();
        static bool login = false;
        static string MemID = "";
        static string Username = "";
        static string FirstName = "";
        static string LastName = "";
        static string Department = "";

        public bool logins(string Username, string Password)
        {
            login = false;
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString);
            using (con)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        sqlcomm = new SqlCommand();
                        sqlcomm.Connection = con;
                        // This will be your input parameter and its value
                        sqlcomm.Parameters.AddWithValue("@Username", Username);
                        sqlcomm.Parameters.AddWithValue("@password", Password);

                        sqlcomm.CommandText = "spr_Login";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            MemID = ds.Tables[0].Rows[0]["memberID"].ToString();
                            Username = ds.Tables[0].Rows[0]["username"].ToString();
                            FirstName = ds.Tables[0].Rows[0]["firstname"].ToString();
                            LastName = ds.Tables[0].Rows[0]["lastname"].ToString();
                            Department = ds.Tables[0].Rows[0]["types"].ToString();
                            login = true;
                        }
                        else
                        {
                            MemID = "";
                            Username = "";
                            FirstName = "";
                            LastName = "";
                            Department = "";
                            login = false;
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
            }
            return login;
        }
        public bool getLogin()
        {
            return login;
        }

        public string getMemID()
        {
            return MemID;
        }

        public string getUsername()
        {
            return Username;
        }

        public string getFirstName()
        {
            return FirstName;
        }

        public string getLastName()
        {
            return LastName;
        }
        public string getDepartment()
        {
            return Department;
        }

        public void ClareLogin()
        {
            login = false;
            string MemID = "";
            string Username = "";
            string FirstName = "";
            string LastName = "";
            string Department = "";
        }
    }
}